import re
import remember.logger
from datetime import datetime

from dateutil.relativedelta import relativedelta


class Time_parser:
    """This class provides few methods for parsing datetimes and relativedelta from stings"""

    log = remember.logger.getLogger('time_parser')

    @staticmethod
    def parse_relative(user_input):
        """Parses string and returns relativedalta as result

        Positional arguments:
        user_input -- string representation of relativedelta in format [n[SMHdm]], where S - seconds, M - minutes,
            H - hours, d- days, m - months
        """
        offsets = {}
        for p in user_input.split():
            if re.match('\d+[SMHdm]', p) and p[-1] not in offsets:
                offsets[p[-1]] = int(p[:-1])
            else:
                return
        relative = relativedelta(
            seconds=offsets.get('S', 0),
            minutes=offsets.get('M', 0),
            hours=offsets.get('H', 0),
            days=offsets.get('d', 0),
            months=offsets.get('m', 0),
        )
        Time_parser.log.debug('Successfully parsed (%s): (%s)', user_input, str(relative))
        return relative

    @staticmethod
    def parse_absolute(user_input):
        """Parses string and returns datetime as result

        Positional arguments:
        user_input -- string representation of datetime in format [dd.mm.yyyy hh:mm:ss]
        """
        user_input = user_input.split()
        date, time = None, None
        if len(user_input) == 2:
            if ':' in user_input[0] or '.' in user_input[1]:
                time = user_input[0]
                date = user_input[1]
            else:
                date = user_input[0]
                time = user_input[1]
        elif len(user_input) == 1:
            if ':' in user_input[0]:
                time = user_input[0]
                date = None
            else:
                date = user_input[0]
                time = None
        dt, pattern = '', ''
        if date is not None:
            matcher_dmy = re.match('^\d{1,2}\.\d{1,2}\.\d{4}$', date)
            matcher_dm = re.match('^\d{1,2}\.\d{1,2}$', date)
            matcher_d = re.match('^\d{1,2}$', date)
            if matcher_dmy:
                pattern += '%d.%m.%Y'
            elif matcher_dm:
                pattern += '%d.%m'
            elif matcher_d:
                pattern += '%d'
            else:
                raise ValueError
            dt += date
        if time is not None:
            matcher_hms = re.match('^\d{1,2}:\d{1,2}:\d{1,2}$', time)
            matcher_hm = re.match('^\d{1,2}:\d{1,2}$', time)
            matcher_h = re.match('^\d{1,2}$', time)
            if matcher_hms:
                pattern += ' %H:%M:%S'
            elif matcher_hm:
                pattern += ' %H:%M'
            elif matcher_h:
                pattern += ' %H'
            else:
                raise ValueError
            dt += ' ' + time
        if '%H' not in pattern:
            pattern += ' %H'
            dt += ' 12'
        if '%m' not in pattern:
            pattern += ' %m'
            dt += ' ' + str(datetime.now().month)
        if '%Y' not in pattern:
            pattern += ' %Y'
            dt += ' ' + str(datetime.now().year)
        if '%d' not in pattern:
            pattern += ' %d'
            dt += ' ' + str(datetime.now().day)
        dt = datetime.strptime(dt, pattern)
        Time_parser.log.debug('Successfully parsed (%s): (%s)', user_input, str(dt))
        return dt
