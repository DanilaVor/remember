from functools import wraps

import remember.logger
import sqlite3


def log_call(func):
    @wraps(func)
    def inner(*args, **kwargs):
        DTO.log.debug('Called with params %s %s', str(args), str(kwargs),
                      extra={'name_override': func.__name__})
        return func(*args, **kwargs)
    return inner


class DTO:
    """Class that connects to the database

    Arguments:
        db_path (str): path to database. Defaulted to info/database.re"""

    __DEFAULT_DB_PATH = "info/database.re"
    MAX_PERCENT = 100
    TWT_CONNECTION_TYPE = 'twt'
    TWF_CONNECTION_TYPE = 'twf'
    FWT_CONNECTION_TYPE = 'fwt'
    FWF_CONNECTION_TYPE = 'fwf'
    RESULT_FAILED = 'failed'
    RESULT_SUCCESS = 'success'
    log = remember.logger.getLogger('DTO')
    
    def __init__(self, db_path=None):
        if db_path is None:
            db_path = DTO.__DEFAULT_DB_PATH
        self.db_path = db_path

    @log_call
    def delete_user(self, mail):
        with sqlite3.connect(self.db_path) as conn:
            conn.execute('PRAGMA foreign_keys = ON')
            cursor = conn.cursor()
            cursor.execute("DELETE FROM Users WHERE mail=?", [mail])
            cursor.execute("SELECT mail FROM Users")
            conn.commit()
            return cursor.fetchone()[0]

    @log_call
    def add_user(self, mail):
        """Adds user record to the database

        Raises:
            sqlite3.IntegrityError: if user with the same mail already exists"""
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            cursor.execute('INSERT INTO Users (mail, name) VALUES (?, ?)', [mail, mail.split('@')[0]])
            conn.commit()

    @log_call
    def get_users_mails(self):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            mails = [x[0] for x in cursor.execute('SELECT mail FROM Users')]
            return mails

    @log_call
    def __try_find_user(self, cursor, mail):
        """Get user id by it's mail

        Returns:
            user id (int)
        Raises:
            UserNotFoundError: if user with this mail doesn't exist"""
        cursor.execute("SELECT id FROM Users WHERE (mail=?)", [mail])
        user_id = cursor.fetchone()
        if user_id is None:
            raise UserNotFoundError()
        return user_id[0]

    @log_call
    def try_find_user(self, mail):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            self.__try_find_user(cursor, mail)

    @log_call
    def add_task(self, mail, task, is_repeat=0):
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            conn.execute('pragma foreign_keys=ON')
            cursor = conn.cursor()
            user_id = self.__try_find_user(cursor, mail)
            assigned_id = self.__try_find_user(cursor, task.assigned)
            cursor.execute("""INSERT INTO Tasks (id, owner, name, description, assigned, priority, scope, status,
                           is_repeat, result) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""",
                           [task.id, user_id, task.name, task.description, assigned_id, task.priority, task.scope,
                            task.status, is_repeat, task.result])
            if task.timelimit is not None:
                tl = task.timelimit
                cursor.execute("INSERT INTO Time_limits (task_id, user_input, type, start, end) VALUES (?, ?, ?, ?, ?)",
                               [task.id, tl.user_input, tl.type, tl.start, tl.end])
            for tag in task.tags:
                cursor.execute("""INSERT INTO Tags (task_id, name) VALUES (?, ?)""", [task.id, tag])
            for comment in task.comments:
                if comment:
                    cursor.execute("""INSERT INTO Comments (task_id, text) VALUES (?, ?)""", [task.id, comment])
            self.add_assigned_owner_connections(cursor, user_id, task.id, task.assigned_connections)
            self.add_public_connections(cursor, user_id, task.id, task.public_connections)
            conn.commit()

    @log_call
    def edit_task(self, mail, task_id, dictionary, tags=None, untags=None, comments=None, timelimit=None,
                  connections=None):
        """Edits task with id like task_id

        Arguments:
            mail (str): mail of owner
            task_id (str): prefix of task id to be found
            dictionary (dict): dict that contains fields to change in format {field_name: field_value}. Expected values
                in dict: [name, description, status, priority, assigned, scope, result]
            tags (list): list of tags to add to the task
            untags (list): list of tags to delete from task
            comments (list): list of comments to add to task
            timelimit: task time limit to save
            connections (dict): list of task connections to other tasks

        Raises:
            UserNotFoundError: if user doesnt exist
            TaskNotFoundError: if task with id like specified doesnt exist
            AmbiguousError: if there are more then one task with id like specified"""
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            conn.execute('pragma foreign_keys=ON')
            cursor = conn.cursor()
            user_id = self.__try_find_user(cursor, mail)
            task_id = self.__try_find_task_owner_assigned(task_id, user_id)

            cursor.execute("SELECT status FROM Tasks WHERE (id=?)", [task_id])
            status = cursor.fetchone()[0]
            if status != DTO.MAX_PERCENT and dictionary.get('result'):
                dictionary['status'] = DTO.MAX_PERCENT
                self.calculate_public_connections(task_id, dictionary.get('result'))
            # TODO CALCULATE SMWHERE HERE

            if dictionary.get('assigned') is not None:
                dictionary['assigned'] = self.__try_find_user(cursor, dictionary['assigned'])

            inserts = ', '.join([str(d) + '="' + str(dictionary[d]) + '"' for d in dictionary.keys()])
            if inserts:
                cursor.execute("UPDATE Tasks SET " + inserts + " WHERE (id=?)", [task_id])
            if tags is not None:
                for tag in tags:
                    cursor.execute("INSERT INTO Tags(task_id, name) VALUES (?, ?)", [task_id, tag])
            if untags is not None:
                for untag in untags:
                    cursor.execute("DELETE FROM Tags WHERE task_id=? AND name=?", [task_id, untag])

            for comment in comments:
                if comment:
                    cursor.execute("INSERT INTO Comments(task_id, text, author_id) VALUES (?, ?, ?)",
                                   [task_id, comment, user_id])
            if timelimit is not None:
                cursor.execute("SELECT id FROM Time_limits WHERE (task_id=?)", [task_id])
                tl_id = cursor.fetchone()
                if tl_id is None:
                    cursor.execute("SELECT MAX (id) FROM Time_limits")
                    tl_id = cursor.fetchone()[0] + 1
                    cursor.execute("""INSERT INTO Time_limits (task_id, user_input, type, start, end)
                                   VALUES (?, ?, ?, ?, ?)""",
                                   [task_id, timelimit.user_input, timelimit.type, timelimit.start, timelimit.end])
                    # cursor.execute("UPDATE Tasks  SET timelimit=? WHERE (id=?)", [tl_id, task_id])

                else:
                    tl_id = tl_id[0]
                    cursor.execute("UPDATE Time_limits SET user_input=?, type=?, start=?, end=? WHERE (id=?)",
                                   [timelimit.user_input, timelimit.type, timelimit.start, timelimit.end, tl_id])
            if connections is not None:
                self.add_public_connections(cursor, user_id, task_id, connections['public connections'])
                self.add_assigned_owner_connections(cursor, user_id, task_id, connections['assigned connections'])
                self.remove_public_connections(cursor, user_id, task_id, connections['public unconnections'])
                self.remove_assigned_owner_connections(cursor, user_id, task_id, connections['assigned unconnections'])
            if dictionary.get('assigned') is not None:
                self.assign_to_children(cursor, task_id, dictionary['assigned'])

            conn.commit()

    @log_call
    def add_public_connections(self, cursor, user_id, task_id, connections):
        for connection in connections:
            if len(task_id) < 30:
                task_id = self.__try_find_task_owner_assigned(task_id, user_id)
            second_id = self.__try_find_public_owner_task(connection[0], user_id)
            cursor.execute("SELECT * FROM Task_connections WHERE (first_id=?) AND (second_id=?)", [second_id, task_id])
            is_found = cursor.fetchone() is not None
            if is_found:
                raise sqlite3.IntegrityError
            cursor.execute("""INSERT INTO Task_connections (first_id, second_id, type)
                            VALUES (?, ?, ?)""", [task_id, second_id, connection[1]])

    @log_call
    def remove_public_connections(self, cursor, user_id, task_id, connections):
        for connection in connections:
            task_id = self.__try_find_task_owner_assigned(task_id, user_id)
            second_id = self.__try_find_public_owner_task(connection[0], user_id)
            cursor.execute("""DELETE FROM Task_connections WHERE (first_id=?) AND (second_id=?) AND (type=?)""",
                           [task_id, second_id, connection[1]])

    @log_call
    def add_assigned_owner_connections(self, cursor, user_id, task_id, connections):
        for connection in connections:
            task_id = self.__try_find_task_owner_assigned(task_id, user_id)
            second_id = self.__try_find_public_owner_task(connection[0], user_id)
            cursor.execute("""SELECT root, depth FROM Task_connections WHERE (first_id=?) AND type="subtask\"""",
                           [task_id])
            first = cursor.fetchone()
            cursor.execute("""SELECT root, depth FROM Task_connections WHERE (first_id=?) AND (type="subtask")""",
                           [second_id])
            second = cursor.fetchone()

            if first is not None and second is not None:
                if first[0] == second[0]:
                    raise InvalidBindingError()
                else:
                    cursor.execute('SELECT depth, root FROM Task_connections WHERE (first_id=?) AND (type="subtask")',
                                   [second_id])
                    d, root = cursor.fetchone()
                    depth = d
                    q = [(task_id, depth + 1)]
                    cursor.execute("""DELETE FROM Task_connections WHERE (first_id=?) AND (type="subtask")""",
                                   [task_id])
                    while q:
                        t_id, depth = q.pop()
                        subtasks = [(x[0], depth + 1) for x in
                                    cursor.execute("""SELECT first_id FROM Task_connections WHERE (second_id=?) AND 
                                                   (type="subtask")""", [t_id])]
                        for s in subtasks:
                            q.insert(0, s)
                        cursor.execute("""UPDATE Task_connections SET root=?, depth=? WHERE (first_id=?)""",
                                       [root, d + 1, t_id])
            elif first is not None:
                root = second_id
                depth = 1
                q = [(task_id, depth + 1)]
                cursor.execute("""DELETE FROM Task_connections WHERE (first_id=?) AND (type="subtask")""",
                               [task_id])
                while q:
                    t_id, depth = q.pop()
                    subtasks = [(x[0], depth + 1) for x in cursor.execute("""SELECT first_id FROM Task_connections 
                                                                          WHERE (second_id=?) AND (type="subtask")""",
                                                                          [t_id])]
                    for s in subtasks:
                        q.insert(0, s)
                    cursor.execute("""UPDATE Task_connections SET root=?, depth=? WHERE (first_id=?)""",
                                   [root, depth, t_id])
                root = second_id
                depth = 1
                pass
            elif second is not None:
                cursor.execute('SELECT depth, root FROM Task_connections WHERE (first_id=?) AND (type="subtask")',
                               [second_id])
                d, r = cursor.fetchone()
                depth, root = d, r
                q = [(task_id, depth + 1)]
                cursor.execute("""DELETE FROM Task_connections WHERE (first_id=?) AND (type="subtask")""",
                               [task_id])
                while q:
                    t_id, depth = q.pop()
                    subtasks = [(x[0], depth + 1) for x in
                                cursor.execute("""SELECT first_id FROM Task_connections WHERE (second_id=?) 
                                               AND (type="subtask")""", [t_id])]
                    for s in subtasks:
                        q.insert(0, s)
                    cursor.execute("""UPDATE Task_connections SET root=?, depth=? WHERE (first_id=?)""",
                                   [root, depth, t_id])
                depth = d + 1
                pass
            else:
                root = second_id
                depth = 1
            cursor.execute("""INSERT INTO Task_connections (first_id, second_id, type, root, depth)
                              VALUES (?, ?, ?, ?, ?)""", [task_id, second_id, "subtask", root, depth])

    @log_call
    def remove_assigned_owner_connections(self, cursor, user_id, task_id, connections):
        for connection in connections:
            task_id = self.__try_find_task_owner_assigned(task_id, user_id)
            second_id = self.__try_find_public_owner_task(connection[0], user_id)
            cursor.execute("""SELECT root FROM Task_connections WHERE (first_id=?) AND (second_id=?)
                           AND (type="subtask")""", [task_id, second_id])
            is_exist = cursor.fetchone() is not None
            if not is_exist:
                # TODO raise?
                return
            q = [(task_id, 0)]
            cursor.execute("""DELETE FROM Task_connections WHERE (first_id=?) AND (type="subtask")""",
                           [task_id])
            while q:
                t_id, depth = q.pop()
                subtasks = [(x[0], depth + 1) for x in
                            cursor.execute("""SELECT first_id FROM Task_connections WHERE (second_id=?) 
                                           AND (type="subtask")""", [t_id])]
                for s in subtasks:
                    q.insert(0, s)
                cursor.execute("""UPDATE Task_connections SET root=?, depth=? WHERE (first_id=?)""",
                               [task_id, depth, t_id])

    @log_call
    def calculate_public_connections(self, task_id, result):
        """Changes result and status of tasks, connected with specified

        Argument:
            task_id (str): task id to find connections
            result (str): result of task to calculated other tasks results"""
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            if result == DTO.RESULT_FAILED:
                cursor.execute("""SELECT first_id FROM Task_connections LEFT JOIN Tasks ON Task_connections.first_id = 
                               Tasks.id WHERE (Tasks.status<?) AND (Task_connections.second_id=?) AND 
                               (Task_connections.type=?)""", [DTO.MAX_PERCENT, task_id, DTO.TWF_CONNECTION_TYPE])
                task_ids = [x[0] for x in cursor.fetchall()]
                for task in task_ids:
                    cursor.execute("UPDATE Tasks SET status=?, result=? WHERE id=?",
                                   [DTO.MAX_PERCENT, DTO.RESULT_SUCCESS, task])
                cursor.execute("""SELECT first_id FROM Task_connections LEFT JOIN Tasks ON Task_connections.first_id = 
                               Tasks.id WHERE (Tasks.status<?) AND (Task_connections.second_id=?) AND 
                               (Task_connections.type=?)""", [DTO.MAX_PERCENT, task_id, DTO.FWF_CONNECTION_TYPE])
                task_ids = [x[0] for x in cursor.fetchall()]
                for task in task_ids:
                    cursor.execute("UPDATE Tasks SET status=?, result=? WHERE id=?",
                                   [DTO.MAX_PERCENT, DTO.RESULT_FAILED, task])
            else:
                cursor.execute("""SELECT first_id FROM Task_connections LEFT JOIN Tasks ON Task_connections.first_id = 
                               Tasks.id WHERE (Tasks.status<?) AND (Task_connections.second_id=?) AND 
                               (Task_connections.type=?)""", [DTO.MAX_PERCENT, task_id, DTO.TWT_CONNECTION_TYPE])
                task_ids = [x[0] for x in cursor.fetchall()]
                for task in task_ids:
                    cursor.execute("UPDATE Tasks SET status=?, result=? WHERE id=?",
                                   [DTO.MAX_PERCENT, DTO.RESULT_SUCCESS, task])
                cursor.execute("""SELECT first_id FROM Task_connections LEFT JOIN Tasks ON Task_connections.first_id = 
                               Tasks.id WHERE (Tasks.status<?) AND (Task_connections.second_id=?) AND 
                               (Task_connections.type=?)""", [DTO.MAX_PERCENT, task_id, DTO.FWT_CONNECTION_TYPE])
                task_ids = [x[0] for x in cursor.fetchall()]
                for task in task_ids:
                    cursor.execute("UPDATE Tasks SET status=?, result=? WHERE id=?",
                                   [DTO.MAX_PERCENT, DTO.RESULT_FAILED, task])

    @log_call
    def delete_subtasks(self, cursor, task_id):
        q = cursor.execute("""SELECT first_id FROM Task_connections WHERE (second_id=?) AND
                           (type="subtask")""", [task_id]).fetchall()
        cursor.execute("""DELETE FROM Task_connections WHERE (second_id=?)""", [task_id])
        while q:
            _id = q.pop()[0]
            ads = cursor.execute("""SELECT first_id FROM Task_connections WHERE (second_id=?) AND
                                           (type="subtask")""", [_id]).fetchall()
            q.extend(ads)
            cursor.execute("""DELETE FROM Task_connections WHERE (second_id=?)""", [_id])
            cursor.execute("""DELETE FROM Tasks WHERE (id=?)""", [_id])

    @log_call
    def assign_to_children(self, cursor, task_id, user_id):
        """Assigns user id to all subtasks in a tree"""
        q = cursor.execute("""SELECT first_id FROM Task_connections WHERE (second_id=?) AND
                                   (type="subtask")""", [task_id]).fetchall()
        cursor.execute("""UPDATE Tasks SET assigned=? WHERE (id=?)""", [user_id, task_id])
        while q:
            _id = q.pop()[0]
            ads = cursor.execute("""SELECT first_id FROM Task_connections WHERE (second_id=?) AND
                                                   (type="subtask")""", [_id]).fetchall()
            q.extend(ads)
            cursor.execute("""UPDATE Tasks SET assigned=? WHERE (id=?)""", [user_id, _id])

    @log_call
    def __try_find_task(self, cursor, id_like, user_id, is_repeat=0):
        """Returns full task id based on prefix, specified user is an owner of the task

        Arguments:
            cursor: cursor to database to execute operations
            id_like (str): prefix of task id
            user_id (int): id of task owner
            is_repeat (int): finds tasks only with specified is_repeat field

        Raises:
            AmbiguousError: if there are more then one task with specified id prefix
            TaskNotFoundError: if there are no tasks with the specified id prefix"""
        id_like += '%'
        cursor.execute("SELECT COUNT(*) FROM Tasks WHERE (id LIKE ?) AND (owner=?) AND (is_repeat=?)",
                       [id_like, user_id, is_repeat])
        count = cursor.fetchone()[0]
        if count > 1:
            raise AmbiguousError()
        if count == 0:
            raise TaskNotFoundError()
        cursor.execute("SELECT id FROM Tasks WHERE (id LIKE ?) AND (owner=?) AND (is_repeat=?)",
                       [id_like, user_id, is_repeat])
        return cursor.fetchone()[0]

    @log_call
    def __try_find_public_owner_task(self, input, user_id=None, is_repeat=0):
        """Returns full task id based on prefix, specified user is an owner of the task or task has "public" scope

        Arguments:
            input (str): prefix of task id
            user_id (int): id of task owner
            is_repeat (int): finds tasks only with specified is_repeat field

        Raises:
            AmbiguousError: if there are more then one task with specified id prefix
            TaskNotFoundError: if there are no tasks with the specified id prefix"""
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            input += '%'
            if user_id is None:
                # TODO what?
                cursor.execute("""SELECT id FROM Tasks WHERE (id LIKE ?) AND (scope="public")  AND (is_repeat=?)""",
                               [input, is_repeat])
            else:
                cursor.execute("""SELECT id FROM Tasks WHERE (id LIKE ?) AND ((scope="public") OR (owner=?))
                               AND (is_repeat=?)""", [input, user_id, is_repeat])
            task_id = cursor.fetchone()
            if task_id is None:
                raise TaskNotFoundError()
            next_id = cursor.fetchone()
            if next_id is not None:
                raise AmbiguousError()
            return task_id[0]

    @log_call
    def __try_find_task_owner_assigned(self, input, user_id, is_repeat=0):
        """Returns full task id based on prefix, specified user is an owner of the task or assigned to it

        Arguments:
            input (str): prefix of task id
            user_id (int): id of task owner
            is_repeat (int): finds tasks only with specified is_repeat field

        Raises:
            AmbiguousError: if there are more then one task with specified id prefix
            TaskNotFoundError: if there are no tasks with the specified id prefix"""
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            if len(input) != 30:
                input += '%'
            cursor.execute("SELECT COUNT(*) FROM Tasks WHERE (id LIKE ?) AND ((owner=?) OR (assigned=?)) AND "
                           "(is_repeat=?)", [input, user_id, user_id, is_repeat])
            count = cursor.fetchone()[0]
            if count > 1:
                raise AmbiguousError()
            if count == 0:
                raise TaskNotFoundError()
            cursor.execute("SELECT id FROM Tasks WHERE (id LIKE ?) AND ((owner=?) OR (assigned=?)) AND (is_repeat=?)",
                           [input, user_id, user_id, is_repeat])
            return cursor.fetchone()[0]

    @log_call
    def delete_task(self, mail, id_like):
        with sqlite3.connect(self.db_path) as conn:
            conn.execute('PRAGMA foreign_keys=ON')
            cursor = conn.cursor()
            user_id = self.__try_find_user(cursor, mail)
            full_id = self.__try_find_task(cursor, id_like, user_id)
            self.delete_subtasks(cursor, full_id)
            cursor.execute("DELETE FROM Tasks WHERE (id=?)", [full_id])

    @log_call
    def get_tasks(self, mail, tags=None):
        """Get dicts of tasks, that have specified tags"""
        if tags is None:
            tags = []
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            user_id = self.__try_find_user(cursor, mail)
            tasks = []
            task_ids = set()
            if not tags:
                task_ids = [task_id[0] for task_id in
                            cursor.execute("SELECT id FROM Tasks WHERE ((assigned=?) OR (owner=?)) AND (is_repeat=0)",
                                           [user_id] * 2)]
            else:
                for tag in tags:
                    ids = [task[0] for task in
                           cursor.execute("""SELECT id FROM Tasks WHERE ((assigned=?) OR (owner=?)) AND (is_repeat=0)
                                          AND EXISTS ( SELECT 1 FROM Tags WHERE task_id=id AND (name=?))""",
                                          [user_id, user_id, tag])]
                    task_ids = task_ids.union(ids)
            for task_id in task_ids:
                tasks.append(self.__get_task_dict(task_id))
        return tasks

    @log_call
    def get_public_tasks(self, mail, tags=None):
        """Get dicts of "public" tasks of specified user, that have specified tags"""
        if tags is None:
            tags = []
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            user_id = self.__try_find_user(cursor, mail)
            if not tags:
                id_s = [x[0] for x in cursor.execute(
                    'SELECT id FROM Tasks WHERE (scope="public") AND ((owner=?) OR (assigned=?)) AND (is_repeat=0)',
                    [user_id] * 2)]
            else:
                id_s = set()
                for tag in tags:
                    ids = [task[0] for task in
                           cursor.execute("""SELECT id FROM Tasks WHERE (scope="public") AND ((assigned=?) OR (owner=?))
                                           AND (is_repeat=0) AND 
                                           EXISTS ( SELECT 1 FROM Tags WHERE task_id=id AND (name=?))""",
                                          [user_id, user_id, tag])]
                    id_s = id_s.union(ids)
            tasks = []
            for task_id in id_s:
                tasks.append(self.__get_task_dict(task_id))
            return tasks

    @log_call
    def __get_task_dict(self, task_id):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            columns = [x[1] for x in cursor.execute("PRAGMA table_info(Tasks)")]
            cursor2 = conn.cursor()
            cursor.execute("SELECT * FROM Tasks WHERE (id=?)", [task_id])
            task = cursor.fetchone()
            dic = {}
            for index, column in enumerate(columns):
                dic[column] = task[index]
            dic.pop('is_repeat')
            tags = [tag[0] for tag in
                    cursor2.execute("SELECT name FROM Tags WHERE (task_id=?)", [dic['id']])]
            comments = [comm[0] for comm in
                        cursor2.execute("SELECT text FROM Comments WHERE (task_id=?)", [dic['id']])]

            connections = \
                cursor2.execute("""SELECT Task_connections.type, Tasks.name, Tasks.id FROM Task_connections 
                                LEFT JOIN Tasks ON Tasks.id = Task_connections.second_id WHERE (first_id=?)""",
                                [dic['id']]).fetchall()
            connected_to = \
                cursor2.execute("""SELECT Task_connections.type, Tasks.name, Tasks.id FROM Task_connections 
                                LEFT JOIN Tasks ON Tasks.id = Task_connections.second_id WHERE (second_id=?)""",
                                [dic['id']]).fetchall()

            # tl_id = cursor2.execute("SELECT time_limit FROM Tasks WHERE (id=?)", [dic['id']]).fetchone()[0]
            # if tl_id is not None:
            # else:
            #    time_limit = None
            try:
                time_limit = self.get_time_limit(dic['id'])
            except NoneFoundError:
                time_limit = None

            dic['owner'] = self.parse_id_to_mail(cursor2, dic, 'owner')
            notifications = []
            notif_ids = [x[0] for x in cursor2.execute('SELECT id FROM Notifications WHERE (task_id=?)', [dic['id']])]
            for notif in notif_ids:
                notifications.append(self.get_notification_dict(notif, dic['owner']))

            # REPEATABLE
            dic['repeats'] = []
            rep_id = cursor2.execute("SELECT id FROM Repeats WHERE (task_id=?)", [dic['id']]).fetchone()
            if rep_id is not None:
                rep_id = rep_id[0]
                repeats = cursor2.execute("""SELECT repeat_index, type, task_id FROM Repeat_tasks_connections WHERE
                                          (repeat_id=?)""", [rep_id]).fetchall()
                for repeat in repeats:
                    if repeat[1] != "deleted":
                        dic['repeats'].append((repeat[0], repeat[1], self.__get_task_dict(repeat[2])))
                    else:
                        dic['repeats'].append((repeat[0], repeat[1], None))

            dic['tags'] = tags
            dic['comments'] = comments
            dic['timelimit'] = time_limit
            dic['notifications'] = notifications
            dic['assigned'] = self.parse_id_to_mail(cursor2, dic, 'assigned')
            dic['connections'] = connections
            dic['connected_to_it'] = connected_to
            return dic

    @log_call
    def find_task(self, input, mail):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            user_id = self.__try_find_user(cursor, mail)
            full_id = self.__try_find_task_owner_assigned(input, user_id)
            # may be error
            return self.__get_task_dict(full_id)

    @log_call
    def parse_id_to_mail(self, cursor, dic, attr):
        if dic[attr] is None:
            return None
        cursor.execute("SELECT mail FROM Users WHERE (id=?)", [dic[attr]])
        return cursor.fetchone()[0]

    @log_call
    def get_timelimits(self):
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            # result = [x[0] for x in
            #           cursor.execute('SELECT time_limit FROM Tasks WHERE (status != 100) AND (time_limit != "NULL")')]
            # tls = []
            # for tl in result:
            #     cursor.execute('SELECT id, end FROM Time_limits WHERE (id=?)', [tl])
            #     tls.append(cursor.fetchone())
            tls = cursor.execute("""SELECT Time_limits.id, Time_limits.end FROM Tasks LEFT JOIN Time_limits ON Tasks.id=
                                 Time_limits.task_id WHERE (Tasks.status != 100) AND (Time_limits.id IS NOT NULL) 
                                 AND NOT EXISTS(SELECT 1 FROM Repeats WHERE Repeats.task_id=Tasks.id)""").fetchall()
            return tls

    @log_call
    def change_time_out_tasks(self, timelimit_ids, result):
        """Gets ids of tasks with overdue time limits, changes their result to specified, status to 100"""
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            to_duplicate = []
            for timelimit in timelimit_ids:
                task_id = cursor.execute('SELECT task_id FROM Time_limits WHERE (id=?)', [timelimit]).fetchone()[0]
                cursor.execute('UPDATE Tasks SET status=?, result=? WHERE id=?', [DTO.MAX_PERCENT, result, task_id])
                is_rep = cursor.execute('SELECT Users.mail, repeat_id FROM Repeat_tasks_connections LEFT JOIN Tasks '
                                        'ON Repeat_tasks_connections.task_id = Tasks.id LEFT JOIN Users '
                                        'ON Tasks.owner=Users.id WHERE (task_id=?)',
                                        [task_id]).fetchone()
                if is_rep is not None:
                    to_duplicate.append(is_rep)
        return to_duplicate

    @log_call
    def get_time_limit(self, task_id):
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            # cursor.execute('SELECT time_limit FROM Tasks WHERE id=?', [task_id])
            result = cursor.execute('SELECT COUNT(*) FROM Time_limits WHERE (task_id=?)', [task_id]).fetchone()[0]
            if result == 0:
                raise NoneFoundError()
            columns = [x[1] for x in cursor.execute("PRAGMA table_info(Time_limits)")]
            cursor.execute('SELECT * FROM Time_limits WHERE (task_id=?)', [task_id])
            tl = cursor.fetchone()
            dic = {}
            for index, column in enumerate(columns):
                dic[column] = tl[index]
            return dic

    @log_call
    def add_notification(self, notification, task_id):
        assert notification is not None
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            cursor.execute('INSERT INTO Notifications(id, task_id, user_input, type, time, text, time_type) '
                           'VALUES (?, ?, ?, ?, ?, ?, ?)',
                           [notification.id, task_id, notification.user_input, notification.type, notification.time,
                            notification.text, notification.time_type])
            conn.commit()

    @log_call
    def get_relative_notification_inputs(self, task_id):
        """Returns: list of tuples with notifications id and user input of notifications with "relative" time type"""
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            result = cursor.execute('SELECT id, user_input FROM Notifications WHERE (task_id LIKE ?) AND '
                                    '(time_type="relative")', [task_id + '%'])
            result = [x for x in result]
        return result

    @log_call
    def change_notifications(self, notifications):
        """Changes notifications times

        Arguments:
            notifications (iterable): tuples of notification id and time to set"""
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            for notification in notifications:
                cursor.execute('UPDATE Notifications SET time=? WHERE (id=?) ', list(reversed(notification)))
            conn.commit()

    @log_call
    def __try_find_notification(self, cursor, input, user_id):
        """Returns full notification id based on prefix, specified user is an owner of the task

        Arguments:
            cursor: cursor to database to execute operations
            input (str): prefix of notification id
            user_id (int): id of notification owner

        Raises:
            AmbiguousError: if there are more then one notification with specified id prefix
            NotificationNotFoundError: if there are no notifications with the specified id prefix"""

        input += '%'
        cursor.execute("""SELECT Notifications.id FROM Notifications LEFT JOIN
                          Tasks ON Notifications.task_id = Tasks.id
                          LEFT JOIN Users ON Tasks.owner = Users.id
                          WHERE (Users.id=?) AND (Notifications.id LIKE ?)""", [user_id, input])
        full_id = cursor.fetchone()
        if full_id is None:
            raise NotificationNotFoundError()
        is_ambiguous = cursor.fetchone()
        if is_ambiguous is not None:
            raise AmbiguousError()
        return full_id[0]

    @log_call
    def get_notification_dict(self, id_like, mail):
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            user_id = self.__try_find_user(cursor, mail)
            full_id = self.__try_find_notification(cursor, id_like, user_id)
            cursor.execute("SELECT * FROM Notifications WHERE (id=?)", [full_id])
            notif = cursor.fetchone()
            columns = [x[1] for x in cursor.execute("PRAGMA table_info(Notifications)")]
            dic = {}
            for index, column in enumerate(columns):
                dic[column] = notif[index]
            return dic

    @log_call
    def delete_notification(self, id_like, mail):
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            user_id = self.__try_find_user(cursor, mail)
            full_id = self.__try_find_notification(cursor, id_like, user_id)
            cursor.execute("DELETE FROM Notifications WHERE (id=?)", [full_id])
            conn.commit()

    @log_call
    def edit_notification(self, id_like, dictionary, mail):
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            user_id = self.__try_find_user(cursor, mail)
            not_id = self.__try_find_notification(cursor, id_like, user_id)
            inserts = ', '.join([str(d) + '="' + str(dictionary[d]) + '"' for d in dictionary.keys()])
            if inserts:
                cursor.execute("UPDATE Notifications SET " + inserts + " WHERE (id=?)", [not_id])
            conn.commit()

    @log_call
    def get_timelimit_by_notify_id(self, id_like, mail):
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            user_id = self.__try_find_user(cursor, mail)
            full_id = self.__try_find_notification(cursor, id_like, user_id)
            cursor.execute("SELECT task_id FROM Notifications WHERE (id=?)", [full_id])
            task_id = cursor.fetchone()[0]
            dic = self.get_time_limit(task_id)
            return dic

    @log_call
    def get_notification_dicts(self):
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            result = cursor.execute("""SELECT Users.mail, Notifications.type, Notifications.time, Notifications.text, 
                                    Tasks.name, Notifications.id FROM Notifications LEFT JOIN
                                    Tasks ON Notifications.task_id = Tasks.id
                                    LEFT JOIN Users ON Tasks.owner = Users.id
                                    WHERE (Notifications.is_send!=1)""")
            notifications = [x for x in result]
            notification_dicts = []
            for notification in notifications:
                dic = {'mail': notification[0], 'type': notification[1], 'time': notification[2],
                       'text': notification[3], 'name': notification[4], 'id': notification[5]}
                notification_dicts.append(dic)
            return notification_dicts

    @log_call
    def mark_notifications(self, notifications):
        """Sets "is send" field of specified notifications to 1"""
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            for notification in notifications:
                cursor.execute("UPDATE Notifications SET is_send=1 WHERE (id=?)", [notification])

    @log_call
    def write_to_dashboard(self, mail, time, _type, text):
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            user_id = self.__try_find_user(cursor, mail)
            cursor.execute("""INSERT INTO Dashboards(user_id, time, type, text) VALUES (?, ?, ?, ?)""",
                           [user_id, time, _type, text])
            conn.commit()

    @log_call
    def clear_dashboard(self, mail):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            user_id = self.__try_find_user(mail)
            cursor.execute("""DELETE FROM Dashboards WHERE (user_id=?)""", [user_id])
            conn.commit()

    @log_call
    def get_dashboard(self, mail):
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            user_id = self.__try_find_user(mail)
            result = cursor.execute("SELECT time, type, text FROM Dashboards WHERE (user_id=?)", [user_id])
            dashboard = []
            for message in result:
                obj = Empty()
                obj.time, obj.type, obj.text = message
                dashboard.append(obj)
            return dashboard

    @log_call
    def __get_columns(self, cursor, table_name):
        columns = [x[1] for x in cursor.execute("PRAGMA table_info(" + table_name + ")")]
        return columns

    # REPEAT TASKS BLOCK

    @log_call
    def add_repeat(self, timedelta, start, end, task_id, current):
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            cursor.execute('INSERT INTO Repeats (delta, start, end, task_id, current_index) VALUES (?, ?, ?, ?, ?)',
                           [timedelta, start, end, task_id, current])

    @log_call
    def add_repeat_tasks_connection(self, template_task_id, repeat_task_id, index):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            rep_id = cursor.execute('SELECT id FROM Repeats WHERE (task_id=?)', [template_task_id]).fetchone()
            if rep_id is None:
                raise NoneFoundError()
            rep_id = rep_id[0]
            print(rep_id, type(rep_id), repeat_task_id, type(repeat_task_id), index, type(index))
            cursor.execute('INSERT INTO Repeat_tasks_connections(repeat_id, task_id, repeat_index) VALUES (?, ?, ?)',
                           [rep_id, repeat_task_id, index])

    @log_call
    def get_repeat(self, task_id_like=None, user_mail=None, repeat_id=None):
        with sqlite3.connect(self.db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as conn:
            cursor = conn.cursor()
            if repeat_id is not None:
                repeat = cursor.execute('SELECT * FROM Repeats WHERE (id=?)', [repeat_id]).fetchone()
            else:
                user_id = self.__try_find_user(cursor, user_mail)
                task_id = self.__try_find_task(cursor, task_id_like, user_id)
                repeat = cursor.execute('SELECT * FROM Repeats WHERE (task_id=?)', [task_id]).fetchone()
            if repeat is None:
                raise NotRepeatableTaskError()
            columns = [x[1] for x in cursor.execute("PRAGMA table_info(Repeats)")]
            dic = {}
            for index, column in enumerate(columns):
                dic[column] = repeat[index]
            return dic

    @log_call
    def get_repeat_id_by_index(self, template_task_id, index, mail):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            print(template_task_id, mail)
            user_id = self.__try_find_user(cursor, mail)
            template_task_id = self.__try_find_task(cursor, template_task_id, user_id)
            rep_id = cursor.execute('SELECT id FROM Repeats WHERE (task_id=?)', [template_task_id]).fetchone()
            if rep_id is None:
                raise NoneFoundError()
            rep_id = rep_id[0]
            task_id = cursor.execute('SELECT repeat_id, type, task_id FROM Repeat_tasks_connections WHERE (repeat_id=?)'
                                     'AND (repeat_index=?)', [rep_id, index]).fetchone()
            if task_id is None:
                return rep_id, None, None
            else:
                return task_id

    @log_call
    def edit_repeat_task(self, repeat_task_id, dictionary):
        print('task id', repeat_task_id)
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            status = \
                cursor.execute('SELECT status FROM Tasks WHERE (id=?) AND (is_repeat=?)',
                               [repeat_task_id, 1]).fetchone()[0]
            if status != DTO.MAX_PERCENT and dictionary.get('result'):
                dictionary['status'] = DTO.MAX_PERCENT
            inserts = ', '.join([str(d) + '="' + str(dictionary[d]) + '"' for d in dictionary.keys()])
            is_edited = False
            for key in dictionary:
                if key != 'status' and key != 'result':
                    is_edited = True
            cursor.execute('UPDATE Tasks SET ' + inserts + ' WHERE (id=?) AND (is_repeat=?)', [repeat_task_id, 1])
            if is_edited:
                repeat_type = 'edited'
            else:
                repeat_type = 'finished'
            cursor.execute('UPDATE Repeat_tasks_connections SET type=? WHERE (task_id=?)', [repeat_type, repeat_task_id])

    @log_call
    def edit_default_future_repeats(self, task_id, dictionary, mail):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            user_id = self.__try_find_user(cursor, mail)
            task_id = self.__try_find_task(cursor, task_id, user_id)
            task_ids = cursor.execute("""SELECT Repeat_tasks_connections.task_id FROM Repeat_tasks_connections 
                                      LEFT JOIN Repeats ON Repeats.id=Repeat_tasks_connections.repeat_id 
                                      WHERE (type IS NULL) AND (Repeats.task_id=?) AND (repeat_index >= current_index)""",
                                      [task_id]).fetchall()
            print(task_ids)
            for task in task_ids:
                self.edit_repeat_task(task[0], dictionary)

    @log_call
    def make_repeat_deleted(self, task_id, mail):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            user_id = self.__try_find_user(cursor, mail)
            cursor.execute('DELETE FROM Tasks WHERE (id=?) AND (owner=?)', [task_id, user_id])
            cursor.execute('UPDATE Repeat_tasks_connections SET type="deleted", task_id="NULL" WHERE (task_id=?)',
                           [task_id])

    # @log_call
    # def delete_one_repeat(self, task_id, mail):
    #     with sqlite3.connect(self.db_path) as conn:
    #         cursor = conn.cursor()
    #         conn.execute('PRAGMA foreign_keys=ON')
    #         user_id = self.__try_find_user(cursor, mail)
    #         cursor.execute('DELETE FROM Repeat_tasks_connections WHERE (task_id=?)', [task_id])
    #         cursor.execute('DELETE FROM Tasks WHERE (id=?) AND (owner=?)', [task_id, user_id])

    @log_call
    def delete_one_repeat(self, repeat_id, index):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            conn.execute('PRAGMA foreign_keys=ON')
            task_id = cursor.execute('SELECT task_id FROM Repeat_tasks_connections WHERE (repeat_id=?) AND '
                                     '(repeat_index=?)', [repeat_id, index]).fetchone()[0]
            if task_id == 'NULL':
                cursor.execute('DELETE FROM Repeat_tasks_connections WHERE (repeat_id=?) AND '
                               '(repeat_index=?)', [repeat_id, index])
            else:
                cursor.execute('DELETE FROM Tasks WHERE (id=?)', [task_id])

    @log_call
    def delete_repeat(self, task_id, mail):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            conn.execute('PRAGMA foreign_keys=ON')
            cursor.execute('DELETE FROM Repeats WHERE (task_id=?)', [task_id])

    @log_call
    def increment_index(self, repeat_id):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            index = cursor.execute('SELECT current_index FROM Repeats WHERE (id=?)', [repeat_id]).fetchone()[0]
            cursor.execute('UPDATE Repeats SET current_index=' + str(index + 1) + ' WHERE (id=?)', [repeat_id])

    @log_call
    def get_repeats(self, repeat_id):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            rows = cursor.execute(
                'SELECT repeat_index, type, task_id FROM Repeat_tasks_connections WHERE (repeat_id=?)',
                [repeat_id]).fetchall()
            return rows

    @log_call
    def update_repeats(self, repeats, repeat_id):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            for repeat in repeats:
                cursor.execute('UPDATE Repeat_tasks_connections SET repeat_index=' + str(repeat[1]) +
                               ' WHERE (repeat_index=?) AND (task_id=?) AND (repeat_id=?)', [repeat[0], repeat[2],
                                                                                             repeat_id])

    @log_call
    def delete_repeats(self, repeats, repeat_id):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            conn.execute('PRAGMA foreign_keys=ON')
            for repeat in repeats:
                cursor.execute('DELETE FROM Repeat_tasks_connections WHERE (repeat_index=?) AND (repeat_id=?)',
                               [repeat, repeat_id])

    @log_call
    def edit_repeat_delta(self, repeat_id, delta):
        with sqlite3.connect(self.db_path) as conn:
            cursor = conn.cursor()
            cursor.execute('UPDATE Repeats SET delta=\"' + delta + '\" WHERE (id=?) ', [repeat_id])


class Empty:
    pass


class AmbiguousError(sqlite3.Error):
    pass


class TaskNotFoundError(sqlite3.Error):
    pass


class UserNotFoundError(sqlite3.Error):
    pass


class NotificationNotFoundError(sqlite3.Error):
    pass


class NoneFoundError(sqlite3.Error):
    pass


class InvalidBindingError(sqlite3.Error):
    pass


class NotRepeatableTaskError(sqlite3.Error):
    pass
