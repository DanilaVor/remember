import logging
import remember.config

DEFAULT_PATH_LOW = 'info/low.log'
DEFAULT_PATH_HIGH = 'info/high.log'

DEBUG = logging.DEBUG
INFO = logging.INFO
WARNING = logging.WARNING
ERROR = logging.ERROR
CRITICAL = logging.CRITICAL
NOTSET = logging.NOTSET
logging.getLogger().setLevel(logging.DEBUG)


def getLogger(name, low_path=None, high_path=None):
    """Returns logger, set to two different files - one for low level messages, one for high level. Is enabled if config
    variable LOGGER_ENABLED is set to True

    Arguments:
    name - logger name, passed to logging.getLogger
    low_path - path to file with low level messages. Default - info/low.log
    high_path - path to file with high level messages. Default - info/high.log"""
    log = logging.getLogger(name)
    log.setLevel(logging.DEBUG)
    log.disabled = not remember.config.LOGGER_ENABLED
    if low_path is None:
        low_path = DEFAULT_PATH_LOW
    if high_path is None:
        high_path = DEFAULT_PATH_HIGH

    formatter = CustomFormatter('%(asctime)s, %(name)s - %(funcName)s [%(levelname)s]: %(message)s')
    formatter2 = CustomFormatter('%(asctime)s, %(name)s - %(funcName)s [%(levelname)s]: %(message)s')

    low_file_handler = logging.FileHandler(low_path)
    low_file_handler.addFilter(LowFilter())
    low_file_handler.setLevel(logging.DEBUG)
    low_file_handler.setFormatter(formatter)

    high_file_handler = logging.FileHandler(high_path)
    high_file_handler.addFilter(HighFilter())
    high_file_handler.setLevel(logging.DEBUG)
    high_file_handler.setFormatter(formatter2)

    log.addHandler(low_file_handler)
    log.addHandler(high_file_handler)
    return log


class CustomFormatter(logging.Formatter):
    """Custom formatter, overrides funcName with value of name_override if it exists"""
    def format(self, record):
        if hasattr(record, 'name_override'):
            record.funcName = record.name_override
        return super(CustomFormatter, self).format(record)


class LowFilter(logging.Filter):
    def filter(self, record):
        return record.levelno <= INFO


class HighFilter(logging.Filter):
    def filter(self, record):
        return record.levelno > INFO
