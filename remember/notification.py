import datetime

from remember.time_parser import Time_parser
import remember.logger


class Notification:
    """Class that describes notification

    Attributes:
        id (str): 30-digit notification id.
        task_id (str): id of owner task.
        user_input (str): user input, that represents datetime of notification
        task_tl (TimeLimit): time limit of owner task
        text (str): text of notification
        time_type (str): "absolute" or "relative", depending on user_input
        type (str): indicates, whether notification should be send on email or on dashboard
        is_send (int): indicates, whether notification was already send"""

    TYPE_DASHBOARD = 'dashboard'
    TYPE_EMAIL = 'email'
    TIME_TYPE_ABSOLUTE = 'absolute'
    TIME_TYPE_RELATIVE = 'relative'
    log = remember.logger.getLogger('Notification')

    def __init__(self, task_id, user_input, task_tl=None, text=None, time=None, time_type=None, type=None, id=None,
                 is_send=None):
        assert task_id is not None
        assert user_input is not None
        self.type = type
        self.time_type = None
        self.user_input = user_input
        self.text = text
        self.task_id = task_id
        if is_send is None:
            self.is_send = 0
        else:
            self.is_send = is_send
        self.time_type = time_type
        if time is None:
            self.time, self.time_type = self.get_date_from_input(user_input, task_tl)
        else:
            self.time, self.time_type = time, type
        if id is None:
            self.id = Notification.get_id(task_id)
        else:
            self.id = id

    @staticmethod
    def get_date_from_input(user_input, task_tl):
        """Parses string, that represents notification date and time, to datetime object

        Arguments:
            user_input (str): user string to parse
            task_tl (TimeLimit): task time limit to calculate relative date time if needed

        Returns:
            tuple of datetime object and notification type"""
        if user_input.startswith('r'):
            if task_tl is None or task_tl.type is None:
                raise TimeLimitTypeError
            relative = Time_parser.parse_relative(user_input[1:])
            relative = task_tl.end - relative
            return relative, Notification.TIME_TYPE_RELATIVE
        elif user_input.startswith('a'):
            return Time_parser.parse_absolute(user_input[1:]), Notification.TIME_TYPE_ABSOLUTE

    @staticmethod
    def get_id(task_id):
        """Returns notification id based on task_id and current time

        Arguments:
            task_id (String) - id of task, owner of notification
        Returns:
            calculated 30-digit id"""
        now = datetime.datetime.now()
        not_id = str(now.microsecond).zfill(6) \
                  + str(now.second).zfill(2) \
                  + str(now.minute).zfill(2) \
                  + str(now.hour).zfill(2) \
                  + str(now.day).zfill(2) \
                  + str(now.month).zfill(2) \
                  + str(now.year % 100).zfill(2) \
                  + str(task_id[-12:])
        Notification.log.debug('Got id %s, task_id - %s, now - %s', task_id, str(now))
        return not_id


class TimeLimitTypeError(ValueError):
    pass
