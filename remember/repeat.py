class Repeat:
    """Class that describes repeat template for repeatable tasks
    Attributes:
        id (int): Repeat id.
        task_id (string): Id of template task.
        start (datetime): start of repeats. Should be set to the timelimit of template task
        end (datetime): end of repeats. Can be set to None
        delta (relativedelta): time delta between repeats
        current_index (int): index of the next repeat to hit time limit
    """
    def __init__(self, id, task_id, start, end, delta, current_index):
        self.task_id = task_id
        self.start = start
        self.end = end
        self.delta = delta
        self.current_index = current_index
        self.id = id
