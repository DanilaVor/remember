from datetime import datetime

import remember.logger
from remember.time_parser import Time_parser


class TimeLimit:
    RELATIVE_TL = 'relative'
    ABSOLUTE_TL = 'absolute'
    DURABLE_TL = 'durable'
    log = remember.logger.getLogger('time_limit')

    def get_date_from_input(self, user_input):
        """Parses string and returns datetimes as result

        Returned values:
        start -- first limit. Can be not-None only in durable time limit
        end -- second limit. Always not-None, if error not raised

        Raised errors:
        ValueError - if time format is not valid

        Positional arguments:
        user_input -- string, represents time limits. Expected formats: absolute timelimit - [a dd.mm.yyyy hh.mm.ss],
            relative - [r [n[SMHdm]], durable - [d dd.mm.yyyy hh.mm.ss - dd.mm.yyyy hh.mm.ss]
        """
        if user_input.startswith('r'):
            self.type = TimeLimit.RELATIVE_TL
            user_input = user_input[1:]
            relative = Time_parser.parse_relative(user_input)
            relative = relative + datetime.now()
            return None, relative
        elif user_input.startswith('a'):
            self.type = TimeLimit.ABSOLUTE_TL
            user_input = user_input[1:]
            return None, Time_parser.parse_absolute(user_input)
        elif user_input.startswith('d'):
            self.type = TimeLimit.DURABLE_TL
            user_input = user_input[1:].split('-')
            if len(user_input) != 2:
                return None, None
            start, end = Time_parser.parse_absolute(user_input[0]), Time_parser.parse_absolute(user_input[1])
            return start, end
        else:
            raise ValueError()

    def __init__(self, user_input, type=None, start=None, end=None, **kwargs):
        TimeLimit.log.debug('Received argument: user_input - %s, type - %s, start - %s, end - %s', user_input, type,
                            str(start), str(end))
        self.type = type
        self.user_input = user_input
        if start is None and end is None:
            self.start, self.end = self.get_date_from_input(user_input)
            TimeLimit.log.debug('Successfully parsed user_input: %s, %s', str(start), str(end))
        else:
            self.start, self.end = start, end