import smtplib
import sys

import remember.config as config
import remember.logger

log = remember.logger.getLogger('mail_sender')


def __send(subject, text, to_address):
    """Sends email with provided subject and text to provided address. Requires login and password for google mail box
    in config.py - config.MAIL_ADDRESS, config.PASSWORD"""
    log.debug('Trying to send mail to %s', to_address)
    host = "smtp.gmail.com"
    body = "\r\n".join((
        "From: {}".format(config.MAIL_ADDRESS),
        "To: {}".format(to_address),
        "Subject: {}".format(subject),
        "",
        text
    ))
    try:
        server = smtplib.SMTP(host, 587)
        server.starttls()
        server.login(config.MAIL_ADDRESS, config.PASSWORD)
        server.sendmail(config.MAIL_ADDRESS, [to_address], body)
        server.quit()
    except smtplib.SMTPException:
        log.error('Got SMTPException - ', sys.exc_info())
    else:
        log.debug('Mail to %s sent successfully', to_address)


def send_notification(mail, task_name, text=None):
    """Sends email on provided mail. If text is not provided, uses default with integrated task name"""
    if text is None:
        text = 'Recalling you about your task - ' + task_name
    __send('Task notification', text, mail)