import remember.dto
import remember.task
import remember.system
import remember.logger


class User:
    """Class that represents user

    Arguments:
        mail (str): email address of user. Should be unique"""

    log = remember.logger.getLogger('user')

    def __init__(self, mail):
        User.log.debug('Received arguments: mail - %s', mail)
        self.mail = mail
        self.name = mail.split('@')[0]
        self.one_repeat_tasks = {}
        self.repeatable_tasks = {}
        self.finished_tasks = {}
        self.notifications = {}
