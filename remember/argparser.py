import argparse
import os
import sys
from datetime import datetime

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append('/home/danilavor/PycharmProjects/project1/venv/lib/python3.5/site-packages')

import remember.logger
from remember.system import System


class Argparser:
    MIN_PRINT_MODE = 0
    MAX_PRINT_MODE = 4
    log = remember.logger.getLogger('argparser')

    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('command', help='Subcommand to run')
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print('Unrecognized command')
            parser.print_help()
            exit(1)
        getattr(self, args.command)()
        System.check_notifications()
        System.check_timelimits()

    def notification(self):
        parser = argparse.ArgumentParser()
        group = parser.add_mutually_exclusive_group()
        group.add_argument('--delete', default=argparse.SUPPRESS)
        group.add_argument('--edit', default=argparse.SUPPRESS)
        group.add_argument('--print', default=argparse.SUPPRESS)
        group.add_argument('--new', default=argparse.SUPPRESS)
        parser.add_argument('-t', '--time')
        parser.add_argument('--type', choices=['email', 'mail', 'm', 'e', 'dashboard', 'd'])
        parser.add_argument('-m', '--message')
        parser.add_argument('--as_user', '--as')

        args = parser.parse_args(sys.argv[2:])
        Argparser.log.info('Args received - %s', str(args))
        if hasattr(args, 'type'):
            if args.type in ['mail', 'm', 'e']:
                args.type = 'email'
            if args.type == 'd':
                args.type = 'dashboard'

        if hasattr(args, 'delete'):
            message = System.delete_notification(args.delete, args.as_user)
            print(message)
        if hasattr(args, 'edit'):
            message = System.edit_notification(args.edit, time=args.time, type=args.type, message=args.message,
                                               mail=args.as_user)
            print(message)
        if hasattr(args, 'print'):
            notification, message = System.get_notification(args.print, args.as_user)
            if message is not None:
                print(message)
                return
            self.print_notification(notification)
        if hasattr(args, 'new'):
            if args.time is None:
                print('Expected time argument')
                return
            message = System.add_notification(args.new, time=args.time, notification_type=args.type,
                                              message=args.message, mail=args.as_user)
            print(message)

    def task(self):
        parser = argparse.ArgumentParser()
        group = parser.add_mutually_exclusive_group()

        group.add_argument('--delete', default=argparse.SUPPRESS)
        group.add_argument('--edit', default=argparse.SUPPRESS)
        group.add_argument('--print', default=argparse.SUPPRESS)
        group.add_argument('--new', action='store_true', default=argparse.SUPPRESS)

        parser.add_argument('-n', '--name')
        parser.add_argument('-d', '--description')
        parser.add_argument('-c', '--comment')
        parser.add_argument('-a', '--assign')
        parser.add_argument('-p', '--priority', type=int, choices=[0, 1, 2, 3, 4, 5])
        parser.add_argument('-s', '--status', type=int)
        parser.add_argument('-m', '--mode', type=int, choices=[0, 1, 2, 3, 4], default=1)
        parser.add_argument('--timelimit', '--tl')
        parser.add_argument('--as_user', '--as')
        parser.add_argument('-r', '--result')
        parser.add_argument('--repeat')
        parser.add_argument('--restore', action='store_true')
        parser.add_argument('--set_repeat')

        parser.add_argument('--connect', nargs=2, action='append', default=[])
        parser.add_argument('--unconnect', nargs=2, action='append', default=[])

        tag_group = parser.add_mutually_exclusive_group()
        tag_group.add_argument('-t', '--tag', default=[])
        tag_group.add_argument('-u', '--untag', default=[])

        args = parser.parse_args(sys.argv[2:])
        Argparser.log.info('Args received - %s', str(args))

        for conn in args.connect:
            if conn[1].lower() not in ['twt', 'fwt', 'twf', 'fwf', 'sub', 'info']:
                print("Expected values ['twt', 'fwt', 'twf', 'fwf', 'sub'] for connect option")
                return
        for conn in args.unconnect:
            if conn[1].lower() not in ['twt', 'fwt', 'twf', 'fwf', 'sub', 'info']:
                print("Expected values ['twt', 'fwt', 'twf', 'fwf', 'sub'] for unconnect option")
                return

        if args.status is not None and not 0 <= args.status <= 100:
            print('Expected status values in range [0,100]')
            return
        if args.result is not None:
            args.result = args.result.lower()
            if args.result == 's':
                args.result = 'success'
            if args.result == 'f':
                args.result = 'fail'
            if args.result == '1':
                args.result = 'success'
            if args.result == '0':
                args.result = 'fail'
            if args.result != 'success' and args.result != 'fail':
                print('Expected "success" or ""fail" value')
                return

        if hasattr(args, 'delete'):
            message = System.remove_task(args.delete, mail=args.as_user, repeat=args.repeat)
            print(message)
        elif hasattr(args, 'new'):
            if args.name is None:
                print('Name argument expected')
                return
            if not args.priority:
                args.priority = 0
            if args.tag:
                args.tag = [x.strip() for x in args.tag.split(',')]
            message = System.add_task(name=args.name,
                                      description=args.description,
                                      comment=args.comment,
                                      priority=args.priority,
                                      assigned=args.assign,
                                      tags=args.tag,
                                      timelimit=args.timelimit,
                                      mail=args.as_user,
                                      connections=args.connect,
                                      unconnections=args.unconnect)
            print(message)
        elif hasattr(args, 'edit'):
            if args.tag:
                args.tags = [x.strip() for x in args.tag.split(',')]
            if args.untag:
                args.untags = [x.strip() for x in args.untag.split(',')]
            message = System.edit_task(args.edit, mail=args.as_user, **args.__dict__)
            print(message)
        elif hasattr(args, 'print'):
            user, task, message = System.find_task(args.print, mail=args.as_user)
            if message:
                print(message)
            else:
                self.print_task(task, args.mode, user, 1)

    def user(self):
        parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
        group = parser.add_mutually_exclusive_group()
        group.add_argument('-n', '--new')
        group.add_argument('-s', '--switch')
        group.add_argument('-t', '--tasks', type=int)
        group.add_argument('-a', '--all', action='store_true')
        group.add_argument('-c', '--current', action='store_true')
        group.add_argument('--delete', nargs='?')
        parser.add_argument('--of')
        parser.add_argument('--as_user', '--as')
        parser.add_argument('--tags')
        args = parser.parse_args(sys.argv[2:])
        Argparser.log.info('Args received - %s', str(args))
        if hasattr(args, 'tags'):
            args.tags = [x.strip() for x in args.tags.split(',')]
        else:
            args.tags = []
        if hasattr(args, 'current'):
            print(System.get_current_user_mail())
        if hasattr(args, 'switch'):
            message = System.switch_user(args.switch)
            print(message)
        if hasattr(args, 'delete'):
            message = System.delete_user(args.delete)
            print(message)
        if hasattr(args, 'all'):
            for user in System.get_users_mails():
                print(user)
        if hasattr(args, 'new'):
            message = System.add_user(args.new)
            print(message)
        if hasattr(args, 'tasks'):
            if not hasattr(args, 'as_user'):
                args.as_user = None
            fin_mode, mul_mode, ort_mode = map(int, (str(args.tasks)).zfill(3))
            mode_range = list(range(Argparser.MIN_PRINT_MODE, Argparser.MAX_PRINT_MODE + 1))
            is_valid = ort_mode in mode_range and mul_mode in mode_range and fin_mode in mode_range
            if not is_valid:
                print('Expected values in range [0,3]')
                return
            if hasattr(args, 'of'):
                user, ort_tasks, mrt_tasks, fin_tasks, msg = System.get_tasks_of(args.of, args.tags)
            else:
                user, ort_tasks, mrt_tasks, fin_tasks, msg = System.get_tasks(args.as_user, args.tags)
            if msg is not None:
                print(msg)
                return
            if ort_mode != 0 and ort_tasks:
                print('One repeat tasks:')
                for task in ort_tasks:
                    self.print_task(task, ort_mode, user, 1)
                print('_________________')
            if mul_mode != 0 and mrt_tasks:
                print('Repeatable tasks:')
                for task in mrt_tasks:
                    self.print_task(task, mul_mode, user, 1)
                print('_________________')
            if fin_mode != 0 and fin_tasks:
                print('Finished tasks:')
                for task in fin_tasks:
                    self.print_task(task, fin_mode, user, 1)

    def dashboard(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-p', '--print', action='store_true', default=argparse.SUPPRESS)
        parser.add_argument('-c', '--clear', action='store_true', default=argparse.SUPPRESS)
        parser.add_argument('--as_user', '--as')
        args = parser.parse_args(sys.argv[2:])
        Argparser.log.info('Args received - %s', str(args))
        if hasattr(args, 'print'):
            dashboard = System.print_dashboard(mail=args.as_user)
            for message in dashboard:
                print(message.time, ' [', message.type, '] ', message.text, sep='')
        if hasattr(args, 'clear'):
            System.clear_dashboard(mail=args.as_user)
            print('Dashboard cleared')

    def print_task(self, task, mode, user, indent):
        def pprint(*x):
            print('\t' * indent, sep='', end='')
            print(*x)
        future_notifications = []
        past_notifications = []
        for notif in task.notifications:
            if notif.time < datetime.now():
                past_notifications.append(notif)
            else:
                future_notifications.append(notif)
        assert 0 <= mode < 5
        if mode > 0:
            if user.mail == task.owner:
                if task.description is not None:
                    s = '{} ({}),'.format(task.name, task.description)
                else:
                    s = "{},".format(task.name)
            else:
                if task.description is not None:
                    s = '\x1b[0;30;43m{} ({}), owner: {}\x1b[0m,'.format(task.name, task.description, task.owner)
                else:
                    s = "\x1b[0;30;43m{}, owner: {}\x1b[0m,".format(task.name, task.owner)
            pprint(s, "priority: {priority}, id: {id}".format(priority=task.priority, id=task.id))
        if mode > 1:
            if task.timelimit is not None:
                if task.timelimit.start is not None:
                    tl = "{} - {}".format(task.timelimit.start, task.timelimit.end)
                else:
                    tl = "{}".format(task.timelimit.end)
                pprint("\tTime limit:", tl)
                pprint("\tAssigned user: {assigned}".format(assigned=task.assigned))
            else:
                pprint("\tAssigned user: {assigned}".format(assigned=task.assigned))
        if mode > 2:
            if task.finished_repeats:
                pprint('\tFinished repeats:')
                for rep in task.finished_repeats:
                    pprint('\t\t{index}) {result}'.format(index=rep[0], result=rep[1].result))
            if task.edited_repeats:
                pprint('\tEdited repeats:')
                for rep in task.edited_repeats:
                    self.print_repeat(rep[0], rep[1], indent+2)
            if task.deleted_repeats:
                pprint('\tDeleted repeats:')
                for rep in task.deleted_repeats:
                    pprint('\t\tNumber', rep[0])
            if not task.comments:
                pprint('\tNo comments yet')
            else:
                pprint('\tComments:')
                for comment in task.comments:
                    pprint('\t\t', comment)
            if not future_notifications:
                pprint('\tNo notifications yet')
            else:
                pprint('\tNotifications:')
                for notification in future_notifications:
                    pprint('\t\t{}, {}'.format(notification.user_input, notification.type))
            if not task.tags:
                pprint('\tNo tags yet')
            else:
                pprint('\tTags:')
                for tag in task.tags:
                    pprint('\t\t', tag)
            if not task.public_connections and not task.assigned_connections:
                pprint('\tNo connections yet')
            else:
                pprint('\tConnections:')
                for conn in task.public_connections:
                    pprint('\t\t{} - {} ({})'.format(*conn))
                for conn in task.assigned_connections:
                    pprint('\t\t{} - {} ({})'.format(*conn))
        if mode > 3:
            if not past_notifications:
                pprint('\tNo past notifications')
            else:
                pprint('\tPast notifications:')
                for notification in past_notifications:
                    pprint('\t\t{}, {}'.format(notification.user_input, notification.type))
            if not task.connected_to_it:
                pprint('\tNo connections to it')
            else:
                pprint('\tConnected to it:')
                for conn in task.connected_to_it:
                    pprint('\t\t{}, {} ({})'.format(*conn))

    def print_repeat(self, index, task, indent):
        def pprint(*x):
            print('\t' * indent, sep='', end='')
            print(*x)
        pprint('{index}) name: {name}, description: {description}, owner: {owner}, assigned: {assigned}'
               .format(index=index, name=task.name, description=task.description, owner=task.owner, assigned=task.assigned))
        pprint('\tstatus: {status}, result: {result}, scope: {scope}'.format(status=task.status, result=task.result,
                                                                             scope=task.scope))

    def print_notification(self, notification):
        print('Task id - {task_id}, id - {id}'.format(task_id=notification.task_id, id=notification.id))
        print("\tWill hit at - {} (user input - '{}')".format(str(notification.time), notification.user_input))
        print('\tType - {}, is send - {}'.format(notification.type, notification.is_send))
        if notification.text is not None:
            print('Text - {}'.format(notification.text))
        else:
            print('\tNo text provided')


if __name__ == '__main__':
    Argparser()
