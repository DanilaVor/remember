import datetime
import hashlib

import remember.logger


class Task:
    """Class that describes tasks

    Attributes:
        id (str): 30-digit task id.
        owner (str): mail of task owner user.
        name (str): user-written name for task
        description (string): user-written description for task
        priority (int): priority if the task
        status (int): percent of task completion
        tags (list): tags of this task
        comments (list): comments for task
        notifications (list of Notification): notifications of this task
        timelimit (TimeLimit): time limit of task
        scope (str): "public" or "private". Describes, can other users read it, or not
        assigned (str): mail of user, assigned to this task. Default should be an owner
        result (str): "failed" or "succeeded". Result of task completion if status is 100
        connections (list): connections to other tasks
        connected_to_it (list): tasks, connected to this
        repeats (list): list of repeat tasks if this is repeatable"""

    log = remember.logger.getLogger('Task')

    def __init__(self, owner, name, description=None, priority=0, status=0, tags=None, comments=None, notifications=None,
                 timelimit=None, scope='public', assigned=None, id=None, result=None, connections=None,
                 connected_to_it=None, repeats=None):
        if notifications is None:
            notifications = []
        if comments is None:
            comments = []
        if tags is None:
            tags = []
        assert owner is not None
        if assigned is None:
            self.assigned = owner.mail
        else:
            self.assigned = assigned
        if id is None:
            self.id = self.get_id(owner)
        else:
            self.id = id
        self.owner = owner
        self.name = name
        self.description = description
        self.priority = priority
        self.status = status
        self.tags = tags
        self.scope = scope
        self.result = result
        self.comments = comments
        self.notifications = notifications
        self.timelimit = timelimit
        if connections is not None:
            self.public_connections = connections['public connections']
            self.assigned_connections = connections['assigned connections']
        else:
            self.public_connections = None
            self.assigned_connections = None
        self.connected_to_it = connected_to_it
        self.edited_repeats = []
        self.deleted_repeats = []
        self.finished_repeats = []
        if repeats is not None and repeats:
            self.is_repeated = True
            for repeat in repeats:
                if repeat[1] == 'deleted':
                    self.deleted_repeats.append((repeat[0], repeat[2]))
                elif repeat[1] == 'edited':
                    self.edited_repeats.append((repeat[0], repeat[2]))
                elif repeat[1] == 'finished':
                    self.finished_repeats.append((repeat[0], repeat[2]))
        else:
            self.is_repeated = False

    @staticmethod
    def get_id(cur_user):
        """Returns task id based on current user name and current time

        Arguments:
            cur_user (User) - current user to take name
        Returns:
            calculated 30-digit id"""
        assert cur_user is not None
        sha = hashlib.sha1(cur_user.name.encode())
        now = datetime.datetime.now()
        task_id = str(now.microsecond).zfill(6) \
                  + str(now.second).zfill(2) \
                  + str(now.minute).zfill(2) \
                  + str(now.hour).zfill(2) \
                  + str(now.day).zfill(2) \
                  + str(now.month).zfill(2) \
                  + str(now.year % 100).zfill(2) \
                  + str(int(sha.hexdigest(), 16))[:12]
        Task.log.debug('Id - %s, now - %s, hash of user - %s', task_id, str(now), str(sha)[:12])
        return task_id
