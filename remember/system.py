import copy
import re
import sqlite3
import sys
import datetime
import smtplib

import remember.dto
import remember.user as _user
import remember.task
import remember.time_limit
import remember.notification
import remember.mail_sender
import remember.time_parser
import remember.repeat
import remember.logger
from dateutil.relativedelta import relativedelta


class System:
    log = remember.logger.getLogger('system')
    DEFAULT_TASK_RESULT = 'failed'
    CURRENT_ID_PATH = 'info/current_mail.re'
    loaded_user = None
    db = remember.dto.DTO()

    @staticmethod
    def get_current_user_mail():
        with open(System.CURRENT_ID_PATH, 'r') as file:
            return file.readlines()[0]

    @staticmethod
    def change_current_user_mail(user_mail):
        with open(System.CURRENT_ID_PATH, 'w') as file:
            file.write(user_mail)

    @staticmethod
    def add_user(mail):
        """Add user with specified mail address and switch to it.

        Returns:
            message(str): Indicates result of operation"""
        assert mail is not None
        if not System.validate_mail(mail):
            System.log.error('Mail %s is not valid', mail)
            return 'Invalid user mail'
        try:
            System.db.add_user(mail)
        except sqlite3.IntegrityError:
            System.log.error('User with mail %s already exists', mail)
            return 'User already exists'
        System.change_current_user_mail(mail)
        System.log.debug('Added %s user', mail)
        return 'User added successfully'

    @staticmethod
    def delete_user(mail):
        """Deletes user with specified mail address and switch user if current was deleted.

        Returns:
            message(str): Indicates result of operation"""
        if mail and not System.validate_mail(mail):
            System.log.error('Mail %s is not valid', mail)
            return 'Invalid user mail'
        if mail is None:
            mail = System.get_current_user_mail()
        try:
            first_user = System.db.delete_user(mail)
        except remember.dto.UserNotFoundError:
            System.log.error('Specified user', mail)
            return 'User not found'
        if mail == System.get_current_user_mail():
            System.change_current_user_mail(first_user)
        System.log.debug('Deleted %s user', mail)
        return 'User deleted successfully'

    @staticmethod
    def switch_user(mail):
        """Switches current to specified.

        Returns:
            message(str): Indicates result of operation"""
        if not System.validate_mail(mail):
            System.log.error('Mail %s is not valid', mail)
            return 'Invalid mail'
        try:
            System.db.try_find_user(mail)
        except remember.dto.AmbiguousError:
            System.log.error('Mail %s is ambiguous', mail)
            return 'Ambiguous input'
        except remember.dto.UserNotFoundError:
            System.log.error('Unable to find %s user', mail)
            return 'Unable to find user'
        System.change_current_user_mail(mail)
        System.log.debug('Switched to %s user', mail)
        return 'User switched'

    @staticmethod
    def get_users_mails():
        mails = System.db.get_users_mails()
        return mails

    @staticmethod
    def add_task(name, description, comment, priority, assigned, tags, timelimit, mail, connections, unconnections):
        if timelimit is not None:
            try:
                timelimit = remember.time_limit.TimeLimit(timelimit)
            except ValueError:
                System.log.error('Unable to parse time limit (%s)', timelimit)
                return 'Expected formats: [a hh:mm:ss dd.mm.yyyy], [r XXS XXM XXH XXd XXm], ' \
                       '[d hh:mm:ss dd.mm.yyyy - hh:mm:ss dd.mm.yyyy]'
        connections = System.get_connections_dict(connections, unconnections)
        try:
            if mail is None:
                mail = System.get_current_user_mail()
            elif not System.validate_mail(mail):
                System.log.error('Mail %s is not valid', mail)
                return 'Invalid mail'
            user = remember.user.User(mail)
            task = remember.task.Task(owner=user,
                                      name=name,
                                      description=description,
                                      comments=[comment],
                                      priority=priority,
                                      assigned=assigned,
                                      tags=tags,
                                      timelimit=timelimit,
                                      connections=connections)
            System.db.add_task(mail, task)
        except remember.dto.AmbiguousError:
            System.log.error('Mail %s is ambiguous', mail)
            return 'Ambiguous input'
        except remember.dto.UserNotFoundError:
            System.log.error('Unable to find %s user', mail)
            return 'Unable to find user'
        except remember.dto.TaskNotFoundError:
            # TODO is it possible?
            System.log.critical('Unable to find %s task', mail)
            return 'Unable to find task'
        except sqlite3.Error:
            System.log.error('Unexpected error - %s', sys.exc_info())
            return 'Unexpected error occurred'
        System.log.debug('Task added successfully (%s %s)', name, description)
        return 'Task added successfully'

    @staticmethod
    def edit_task(input, mail, name=None, description=None, priority=None, status=None, comment=None, scope=None,
                  assign=None, tags=None, untags=None, result=None, timelimit=None, connect=None,
                  unconnect=None, repeat=None, set_repeat=None, restore=None, **kwargs):
        assert input is not None
        if mail is None:
            mail = System.get_current_user_mail()
        elif not System.validate_mail(mail):
            return 'Invalid user mail'
        args_dict = {}
        if kwargs:
            System.log.warn('Unexpected options received %s', str(kwargs))
        if name is not None:
            args_dict['name'] = name
        if description is not None:
            args_dict['description'] = description
        if assign is not None:
            args_dict['assigned'] = assign
        if priority is not None:
            args_dict['priority'] = priority
        if status is not None:
            args_dict['status'] = status
        if scope is not None:
            args_dict['scope'] = scope
        if result is not None:
            args_dict['result'] = result
        System.log.debug('Got args dict %s', str(args_dict))
        if timelimit is not None:
            try:
                timelimit = remember.time_limit.TimeLimit(timelimit)
            except ValueError:
                System.log.error('Unable to parse time limit (%s)', timelimit)
                return 'Expected formats: [a hh:mm:ss dd.mm.yyyy], [r XXS XXM XXH XXd XXm], ' \
                       '[d hh:mm:ss dd.mm.yyyy - hh:mm:ss dd.mm.yyyy]'
        connections = System.get_connections_dict(connect, unconnect)
        if repeat is not None:
            if comment is not None or tags is not None or untags is not None or timelimit is not None:
                System.log.error('Unexpected option provided', comment or tags or untags or timelimit)
                return 'Unexpected option provided'
            try:
                repeat_id, index, task_id = System.get_one_repeat_id(input, repeat, mail)
            except remember.dto.UserNotFoundError:
                System.log.error('Unable to find %s user', mail)
                return 'Unable to find user'
            except remember.dto.TaskNotFoundError:
                System.log.error('Unable to find task (%s)', input)
                return 'Unable to find task'
            except remember.dto.NotRepeatableTaskError:
                System.log.error('Task %s is not repeatable', input)
                return 'Task is not repeatable'
            except ValueError:
                System.log.error('Unable to parse time limit (%s)', timelimit)
                return 'Unavailable date format'
            if restore:
                System.db.delete_one_repeat(repeat_id, index)
                System.get_one_repeat_id(input, repeat, mail)
            else:
                if task_id is None:
                    return 'This repeat was deleted. To change, restore it first'
                System.db.edit_repeat_task(task_id, args_dict)
        else:
            try:
                System.db.edit_task(mail, input, args_dict, tags=tags, untags=untags, comments=[comment],
                                    timelimit=timelimit, connections=connections)
            # TODO check seven times and change
            except remember.dto.UserNotFoundError:
                System.log.error('Unable to find %s user', mail)
                return 'Unable to find user'
            except remember.dto.TaskNotFoundError:
                System.log.error('Unable to find task (%s)', input)
                return 'Unable to find task'
            except sqlite3.IntegrityError:
                System.log.error('Integrity error (%s or %s)', str(tags), str(connections))
                return 'One of tags or connections already exists'
            except sqlite3.Error:
                System.log.error('Unexpected error occurred (%s)', sys.exc_info())
                return sys.exc_info()[1]
            System.db.edit_default_future_repeats(input, args_dict, mail)
            if timelimit is not None:
                System.recalculate_timelimits(input, timelimit)
        if set_repeat is not None:
            user, task, message = System.find_task(input, mail)
            System.make_repeatable(task.id, set_repeat, mail)
        System.log.debug('Task edited successfully (%s)', input)
        return 'Task edited successfully'

    @staticmethod
    def get_connections_dict(connections, unconnections):
        System.log.debug('Method called with parameters %s, %s', connections, unconnections)
        dic = {'public connections': [], 'assigned connections': [], 'public unconnections': [],
               'assigned unconnections': []}
        if connections is not None:
            for connection in connections:
                if connection[1] in ['sub', 'subtask']:
                    dic['assigned connections'].append(connection)
                else:
                    dic['public connections'].append(connection)
        if unconnections is not None:
            for unconnection in unconnections:
                if unconnection[1] in ['sub', 'subtask']:
                    dic['assigned unconnections'].append(unconnection)
                else:
                    dic['public unconnections'].append(unconnection)
        return dic

    @staticmethod
    def remove_task(id_like, mail, repeat=None):
        System.log.info('Method called with params id_like-%s, mail-%s, repeat-%s', id_like, mail, repeat)
        if mail is None:
            mail = System.get_current_user_mail()
        elif not System.validate_mail(mail):
            System.log.error('Unable to find %s user', mail)
            return 'Invalid mail'
        if repeat is not None:
            # print(repeat)
            try:
                repeat_id, index, task_id = System.get_one_repeat_id(id_like, repeat, mail)
            except remember.dto.UserNotFoundError:
                System.log.error('User with mail %s cant be found', mail)
                return 'Unable to find user'
            except remember.dto.TaskNotFoundError:
                System.log.error('Task with id like %s cant be found', id_like)
                return 'Unable to find task'
            except remember.dto.NotRepeatableTaskError:
                System.log.error('Task with id like %s is not repeatable', id_like)
                return 'Task is not repeatable'
            # print(task_id)
            if task_id is not None:
                System.db.make_repeat_deleted(task_id, mail)
        else:
            try:
                System.db.delete_task(mail, id_like)
            except remember.dto.AmbiguousError:
                System.log.error('Task id like %s is ambiguous', id_like)
                return 'Ambiguous input'
            except remember.dto.TaskNotFoundError:
                System.log.error('Task with id like %s cant be found', id_like)
                return 'Unable to find task'
        System.log.info('Task %s removed successfully', id_like)
        return 'Task removed successfully'

    @staticmethod
    def validate_mail(mail):
        """Validates mail based on regexp
        Returns:
            True or False - result of validation"""
        if re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", mail):
            return True
        return False

    @staticmethod
    def get_tasks(mail, tags):
        """Get tasks of specified user

        Returns:
            user object with specified mail
            3 lists of tasks - one repeat, repeatable, finished tasks
            message - not None if an error occurred"""
        if mail is None:
            mail = System.get_current_user_mail()
        elif not System.validate_mail(mail):
            System.log.error('Mail %s is not valid', mail)
            return None, None, None, None, 'Invalid mail'
        user = remember.user.User(mail)
        task_dicts = System.db.get_tasks(mail, tags)
        tasks = []
        for task in task_dicts:
            tasks.append(System.parse_task_dict(task))
        ort_tasks = []
        mrt_tasks = []
        fin_tasks = []
        for task in tasks:
            if task.is_repeated:
                mrt_tasks.append(task)
            elif task.status == 100:
                fin_tasks.append(task)
            else:
                ort_tasks.append(task)
        System.log.debug('Tasks parsed successfully')
        return user, ort_tasks, mrt_tasks, fin_tasks, None

    @staticmethod
    def get_tasks_of(mail, tags):
        """Get public tasks of specified user

        Returns:
            user object with specified mail
            3 lists of tasks - one repeat, repeatable, finished tasks
            message - not None if an error occurred"""
        assert mail is not None
        if not System.validate_mail(mail):
            System.log.error('Mail %s is not valid', mail)
            return None, None, None, None, 'Invalid mail'
        user = remember.user.User(mail)
        task_dicts = System.db.get_public_tasks(mail, tags)
        tasks = []
        for task in task_dicts:
            tasks.append(System.parse_task_dict(task))
        ort_tasks = []
        mrt_tasks = []
        fin_tasks = []
        for task in tasks:
            if task.is_repeated:
                mrt_tasks.append(task)
            elif task.status == 100:
                fin_tasks.append(task)
            else:
                ort_tasks.append(task)
        System.log.debug('Tasks parsed successfully')
        return user, ort_tasks, mrt_tasks, fin_tasks, None

    @staticmethod
    def parse_task_dict(dic):
        """Parses task dict to task object"""
        if dic['timelimit'] is not None:
            dic['timelimit'].pop('id')
            dic['timelimit'] = remember.time_limit.TimeLimit(**dic['timelimit'])
        conn_dic = {'public connections': [], 'assigned connections': []}
        repeats = []
        for repeat in dic['repeats']:
            if repeat[1] != 'deleted':
                repeats.append((repeat[0], repeat[1], System.parse_task_dict(repeat[2])))
            else:
                repeats.append((repeat[0], repeat[1], None))
        dic['repeats'] = repeats
        for conn in dic['connections']:
            if conn[0] in ['sub', 'subtask']:
                conn_dic['assigned connections'].append(conn)
            else:
                conn_dic['public connections'].append(conn)
        notifications = []
        dic['connections'] = conn_dic
        for notif in dic['notifications']:
            notifications.append(remember.notification.Notification(**notif))
        dic['notifications'] = notifications
        task = remember.task.Task(**dic)
        return task

    @staticmethod
    def find_task(input, mail):
        """Get task with specified id prefix and owner

        Returns:
            user object with specified mail
            task object
            message - None if error occurred"""
        assert input is not None
        if mail is None:
            mail = System.get_current_user_mail()
        elif not System.validate_mail(mail):
            return None, None, 'Invalid mail'
        user = remember.user.User(mail)
        try:
            task = System.db.find_task(input, mail)
        except remember.dto.UserNotFoundError:
            System.log.error('User with mail %s cant be found', mail)
            return None, None, 'Unable to find user'
        except remember.dto.TaskNotFoundError:
            System.log.error('Task with id %s cant be found', input)
            return None, None, 'Unable to find task'
        except remember.dto.AmbiguousError:
            System.log.error('Task id %s is ambiguous', input)
            return None, None, 'Ambiguous input'
        System.log.debug('Task with id %s was found, %s', input, str(task))
        return user, System.parse_task_dict(task), None

    @staticmethod
    def check_timelimits():
        timelimits = System.db.get_timelimits()
        to_change = []
        for timelimit in timelimits:
            if timelimit[1] < datetime.datetime.now():
                to_change.append(timelimit[0])
        # print('to change -', to_change)
        to_duplicate = System.db.change_time_out_tasks(to_change, System.DEFAULT_TASK_RESULT)
        # print('to duplicate -', to_duplicate)
        System.log.info('Tasks to change - %s', str(to_change))
        System.log.info('Tasks to duplicate - %s', str(to_duplicate))
        for dupl in to_duplicate:
            System.add_next_repeat(*dupl)

    @staticmethod
    def add_notification(task_like, time, mail, notification_type=None, message=None, ):
        if mail is None:
            mail = System.get_current_user_mail()
        elif not System.validate_mail(mail):
            System.log.error('Mail %s is not valid', mail)
            return 'Invalid mail'
        if notification_type is None:
            notification_type = 'dashboard'
        try:
            task = System.find_task(task_like, mail)
        except remember.dto.UserNotFoundError:
            System.log.error('User with mail %s cant be found', mail)
            return 'Unable to find user'
        except remember.dto.TaskNotFoundError:
            System.log.error('Task with id like %s cant be found', task_like)
            return 'Unable to find task'
        except remember.dto.AmbiguousError:
            System.log.error('Task id like %s is ambiguous', task_like)
            return 'Ambiguous input'
        try:
            tl = System.get_timelimit(task.id)
        except ValueError:
            System.log.error('Unable to parse time time limit for task %s', task_like)
            return 'Unable to parse timelimit'
        try:
            notification = remember.notification.Notification(task_id=task.id,
                                                              user_input=time,
                                                              task_tl=tl,
                                                              type=notification_type,
                                                              text=message)
        except remember.notification.TimeLimitTypeError:
            System.log.error('Cant add notification cause task %s is not repeatable', task.id)
            return "Task doesn't have time limit, unable to calculate relative notification time"
        except ValueError:
            System.log.error('Time cant be parsed %s', time)
            return 'Expected formats: [a hh:mm:ss dd.mm.yyyy], [r XXS XXM XXH XXd XXm], ' \
                   '[d hh:mm:ss dd.mm.yyyy - hh:mm:ss dd.mm.yyyy]'
        try:
            System.db.add_notification(notification, task.id)
        except sqlite3.IntegrityError:
            System.log.error('Notification with the id %s already exists', notification.id)
            return 'Notification with the same id already exists'
        System.log.debug('Notification added successfully (%s)', notification.id)
        return 'Notification added successfully'

    @staticmethod
    def get_timelimit(task_id):
        tl_dict = System.db.get_time_limit(task_id)
        if tl_dict is None:
            return
        tl = remember.time_limit.TimeLimit(tl_dict['user_input'], tl_dict['type'], tl_dict['start'], tl_dict['end'])
        System.log.info('Time limit for %s parsed successfully', task_id)
        return tl

    @staticmethod
    def recalculate_timelimits(task_id, time_limit):
        """Changes all "relative" notifications times

        Arguments:
            task_id (str): id of task with changed time limit
            time_limit: new time limit for task"""
        notifications = System.db.get_relative_notification_inputs(task_id)
        to_change = []
        System.log.debug('Time limits to change %s', str(to_change))
        for notification in notifications:
            notif = remember.notification.Notification(task_id, time_limit, notification[1])
            to_change.append([notification[0], notif.time])
        System.db.change_notifications(to_change)

    @staticmethod
    def get_notification(id_like, mail):
        assert id_like is not None
        if mail is None:
            mail = System.get_current_user_mail()
        elif not System.validate_mail(mail):
            System.log.error('User mail %s is not valid', mail)
            return 'Invalid mail'

        try:
            notification_dict = System.db.get_notification_dict(id_like, mail)
            notification = remember.notification.Notification(**notification_dict)
        except remember.dto.UserNotFoundError:
            System.log.error('User with mail %s cant be found', mail)
            return None, 'Unable to find user'
        except remember.dto.NotificationNotFoundError:
            System.log.error('Task with id like %s cant be found', id_like)
            return None, 'Unable to find notification'
        except remember.dto.AmbiguousError:
            System.log.error('Task id like %s is ambiguous', id_like)
            return None, 'Ambiguous input'
        return notification, None

    @staticmethod
    def delete_notification(id_like, mail):
        if mail is None:
            mail = System.get_current_user_mail()
        elif not System.validate_mail(mail):
            System.log.error('User mail %s is not valid', mail)
            return 'Invalid mail'
        try:
            System.db.delete_notification(id_like, mail)
        except remember.dto.UserNotFoundError:
            System.log.error('User with mail %s cant be found', mail)
            return 'Unable to find user'
        except remember.dto.NotificationNotFoundError:
            System.log.error('Notification with id like %s cant be found', id_like)
            return 'Unable to find notification'
        except remember.dto.AmbiguousError:
            System.log.error('Notification id like %s is ambiguous', id_like)
            return 'Ambiguous input'
        return 'Notification deleted successfully'

    @staticmethod
    def edit_notification(id_like, mail, time=None, type=None, message=None):
        if mail is None:
            mail = System.get_current_user_mail()
        elif not System.validate_mail(mail):
            System.log.error('User mail %s is not valid', mail)
            return 'Invalid mail'
        try:
            tl_dict = System.db.get_timelimit_by_notify_id(id_like, mail)
        except remember.dto.UserNotFoundError:
            System.log.error('User with mail %s cant be found', mail)
            return 'Unable to find user'
        except remember.dto.NotificationNotFoundError:
            System.log.error('Notification with id like %s cant be found', id_like)
            return 'Unable to find notification'
        except remember.dto.AmbiguousError:
            System.log.error('Notification id like %s is ambiguous', id_like)
            return 'Ambiguous input'
        if tl_dict is not None:
            tl = remember.time_limit.TimeLimit(tl_dict['user_input'], tl_dict['type'], tl_dict['start'], tl_dict['end'])
        else:
            tl = None
        dictionary = {}
        if time is not None:
            try:
                dictionary['user_input'] = time
                dictionary['time'], dictionary['time_type'] = remember.notification.Notification.get_date_from_input(
                    time, tl)
            except ValueError:
                System.log.error('Cant parse time (%s)', time)
                return 'Expected formats: [a hh:mm:ss dd.mm.yyyy], [r XXS XXM XXH XXd XXm], ' \
                       '[d hh:mm:ss dd.mm.yyyy - hh:mm:ss dd.mm.yyyy]'
        if type is not None:
            dictionary['type'] = type
        if message is not None:
            dictionary['text'] = message
        try:
            System.db.edit_notification(id_like, dictionary, mail)
        except remember.dto.NotificationNotFoundError:
            System.log.error('Notification with id like %s cant be found', id_like)
            return 'Unable to find notification'
        except remember.dto.AmbiguousError:
            System.log.error('Notification id like %s is ambiguous', id_like)
            return 'Ambiguous input'
        System.log.info('Notification with id like %s was edited successfully', id_like)
        return 'Notification edited successfully'

    @staticmethod
    def check_notifications():
        """Checks whether any notification has to be send"""
        notifications = System.db.get_notification_dicts()
        to_change = []
        for notif in notifications:
            if notif['time'] < datetime.datetime.now():
                if notif['type'] == remember.notification.Notification.TYPE_DASHBOARD:
                    System.db.write_to_dashboard(notif['mail'],
                                                 datetime.datetime.now(),
                                                 DashboardMessages.NOTIFICATION,
                                                 DashboardMessages.get_notification_text(notif['name']))
                    to_change.append(notif['id'])
                else:
                    try:
                        remember.mail_sender.send_notification(notif['mail'], notif['name'], notif['text'])
                        to_change.append(notif['id'])
                    except smtplib.SMTPConnectError:
                        System.log.error('While attempting to send mail - ', str(sys.exc_info()))
        System.log.info('Notifications to send - %s', str(to_change))
        System.db.mark_notifications(to_change)

    @staticmethod
    def print_dashboard(mail):
        if mail is None:
            mail = System.get_current_user_mail()
        dashboard = System.db.get_dashboard(mail)
        return dashboard

    @staticmethod
    def clear_dashboard(mail):
        if mail is None:
            mail = System.get_current_user_mail()
        if not System.validate_mail(mail):
            System.log.error('User mail %s is not valid', mail)
            return
        System.db.clear_dashboard(mail)
        System.log.info('Dashboard for %s was cleared', mail)

    # REPEAT TASKS BLOCK

    @staticmethod
    def make_repeatable(task_id, input, mail=None):
        if mail is None:
            mail = System.get_current_user_mail()
        elif not System.validate_mail(mail):
            System.log.error('User mail %s is not valid', mail)
            return 'Invalid user mail'
        user, task, message = System.find_task(task_id, mail)
        if input == "" or input is None:
            System.db.delete_repeat(task_id, mail)
            return
        delim = [s.strip() for s in input.split()]
        set_repeat = {delim[i]: delim[i + 1] for i in range(0, len(delim), 2)}
        try:
            if set_repeat.get('to'):
                end = remember.time_parser.Time_parser.parse_absolute(set_repeat['to'])
                set_repeat.pop('to')
            else:
                end = None
            if set_repeat.get('by'):
                delta = remember.time_parser.Time_parser.parse_relative(set_repeat['by'])
                set_repeat.pop('by')
            else:
                delta = relativedelta(days=1)
        except ValueError:
            System.log.error('Couldnt parse relative - %s', input)
            return 'Unexpected date format'
        try:
            repeat = System.db.get_repeat(task_id, mail)
        except remember.dto.NotRepeatableTaskError:
            if task.timelimit is None:
                System.log.error('Task has not repeatable')
                return "Can't make repeatable task without time limit"
            if task.notifications:
                min_notif_time = None
                for notif in task.notifications:
                    if min_notif_time is None:
                        min_notif_time = notif.time
                    elif notif.time < min_notif_time:
                        min_notif_time = notif.time
                count = 1
                while min_notif_time < datetime.datetime.now() and min_notif_time < end:
                    count += 1
                    min_notif_time += delta
            else:
                count = 1
                time = task.timelimit.end
                while time < datetime.datetime.now() and time < end:
                    time += delta
                    count += 1
            current = 0
            tl = task.timelimit.end
            while tl < datetime.datetime.now() and tl < end:
                tl += delta
                current += 1
            delta_str = str(delta).split('(')[1][:-1]
            System.db.add_repeat(delta_str, task.timelimit.end, end, task.id, current)
            for i in range(0, count):
                copied_task = System.duplicate_task(user, task, delta * i)
                System.db.add_task(task.owner, copied_task, 1)
                for notif in copied_task.notifications:
                    System.db.add_notification(notif, copied_task.id)
                System.db.add_repeat_tasks_connection(task.id, copied_task.id, i)
        else:
            to_delete = []  # (old index)
            to_change = []  # (old index, new index)
            repeats = System.db.get_repeats(repeat['id'])
            rel_dict = {d.split('=')[0].strip(): int(d.split('=')[1]) for d in repeat['delta'].split(',')}
            repeat['delta'] = relativedelta(**rel_dict)
            old_delta = repeat['delta']
            start = repeat['start'].date()
            next_index = 1
            for task_repeat in repeats:
                if task_repeat[0] < repeat['current_index']:
                    continue
                # -1 or not?
                new_delta = start  # + old_delta*(repeat['current_index'])
                while new_delta < start + task_repeat[0] * old_delta:
                    new_delta += delta
                if new_delta == start + task_repeat[0] * old_delta:
                    to_change.append((task_repeat[0], next_index + repeat['current_index'], task_repeat[2]))
                    next_index += 1
                else:
                    if task_repeat[0] == repeat['current_index']:
                        next_index -= 1
                    to_delete.append(task_repeat[0])
            System.db.delete_repeats(to_delete, repeat['id'])
            System.db.update_repeats(to_change, repeat['id'])
            System.db.edit_repeat_delta(repeat['id'], str(delta).split('(')[1][:-1])
            for i in range(next_index + repeat['current_index']):
                System.get_one_repeat_id(task.id, '№' + str(i), mail)

    @staticmethod
    def duplicate_task(user, task, delta):
        print('Task result, status', task.result, task.status)
        copy_task = copy.deepcopy(task)
        copy_task.id = remember.task.Task.get_id(user)
        copy_task.comments = []
        copy_task.tags = []
        for notif in copy_task.notifications:
            notif.time += delta
            notif.user_input = ""
            notif.id = remember.notification.Notification.get_id(copy_task.id)
            notif.is_send = 0
        if copy_task.timelimit.end is not None:
            copy_task.timelimit.end += delta
        if copy_task.timelimit.start is not None:
            copy_task.timelimit.start += delta
        print('Copies task result, status', copy_task.result, copy_task.status)
        return copy_task

    @staticmethod
    def get_one_repeat_id(task_id_like, input, mail=None):
        """Get id of specified repeat of specified task"""
        if mail is None:
            mail = System.get_current_user_mail()
        elif not System.validate_mail(mail):
            return 'User mail is not valid'
        repeat_dict = System.db.get_repeat(task_id_like, mail)
        rel_dict = {d.split('=')[0].strip(): int(d.split('=')[1]) for d in repeat_dict['delta'].split(',')}
        repeat_dict['delta'] = relativedelta(**rel_dict)
        repeat = remember.repeat.Repeat(**repeat_dict)
        if input.startswith('№'):
            index = int(input[1:])
            if repeat.start.date() + index * repeat.delta > repeat.end.date():
                return 'This date is not covered by pattern'
        else:
            date = remember.time_parser.Time_parser.parse_absolute(input)
            date = date.date()
            count = 1
            start = repeat.start.date()
            while date > start:
                start += repeat.delta
                count += 1
            if date != start or start > repeat.end.date():
                return 'This date is not covered by pattern'
            index = count
        # HERE
        repeat_id, type, task_id = System.db.get_repeat_id_by_index(task_id_like, index, mail)
        if task_id is None and type != 'deleted':
            task_dict = System.db.find_task(task_id_like, mail)
            task = System.parse_task_dict(task_dict)
            copied_task = System.duplicate_task(remember.user.User(mail), task, repeat.delta * index)
            System.db.add_task(task.owner, copied_task, 1)
            for notif in copied_task.notifications:
                System.db.add_notification(notif, copied_task.id)
            System.db.add_repeat_tasks_connection(task.id, copied_task.id, index)
            task_id = copied_task.id
        return repeat_id, index, task_id

    @staticmethod
    def add_next_repeat(mail, repeat_id):
        repeat = System.db.get_repeat(repeat_id=repeat_id)
        System.db.increment_index(repeat_id)
        delta_dict = {d.split('=')[0].strip(): int(d.split('=')[1]) for d in repeat['delta'].split(',')}
        repeat['delta'] = relativedelta(**delta_dict)
        task = System.find_task(repeat['task_id'], mail)[1]
        task_id = System.db.get_repeat_id_by_index(task.id, repeat['current_index'] + 1, mail)
        if task_id is not None:
            return
        copy = System.duplicate_task(remember.user.User(mail), task,
                                     repeat['delta'] * int((repeat['current_index'] + 1)))
        System.db.add_task(mail, copy, 1)
        System.db.add_repeat_tasks_connection(task.id, copy.id, repeat['current_index'] + 1)


class DashboardMessages:
    NOTIFICATION = 'NOTIFICATION'
    ASSIGN = 'ASSIGN'
    TIME_LIMIT = 'TIME_LIMIT'

    @staticmethod
    def get_notification_text(task_name):
        """Returns default notification message based on task name"""
        text = task_name + ' hit the notification time'
        return text

    @staticmethod
    def get_assign_text(task_name):
        """Returns default assigning notification message based on task name"""
        text = 'You were assigned to the ' + task_name
        return text
