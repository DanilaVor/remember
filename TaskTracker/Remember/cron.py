import sys
from datetime import datetime

from django_cron import CronJobBase, Schedule

from .system import System


class CronJob(CronJobBase):
    RUN_EVERY_MINS = 1

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'my_app.my_cron_job'    # a unique code

    def do(self):
        try:
            System.check_timelimits()
            print('TL, ', end='')
            System.check_notifications()
            print('Notifications', datetime.now())
        except Exception:
            print(sys.exc_info())