from datetime import datetime

from dateutil.relativedelta import relativedelta as rel_delta
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.utils import timezone

from . import consts
from .models.notification import Notification
from .models.task import Task, Tag, Repeat, TaskRepeatConnection, Comment, SubtaskConnection


class System:
    @staticmethod
    def get_tasks(user, name, finished, tags=None):
        if tags is None:
            tags = []
        query = Task.objects.filter(Q(owner=user)|Q(assigned=user), name__contains=name).filter(repeat=None)
        if finished:
            query = query.filter(status=consts.MAX_STATUS)
        else:
            query = query.exclude(status=consts.MAX_STATUS)
        for tag in tags:
            tag = tag.strip()
            if tag:
                query = query.filter(tag__name__iexact=tag)
        return query

    @staticmethod
    def update_repeat(task, name, description=None, assigned=None, priority=None, status=None, result=None, tags=None):
        task.name = name
        task.repeat.rep_type = TaskRepeatConnection.EDITED_REPEAT_TYPE
        task.repeat.save()
        task.description = description
        task.assigned = assigned
        task.priority = priority
        task.status = status
        task.result = result
        for tag in tags:
            if not Tag.objects.filter(name=tag, task=task).exists():
                Tag.objects.create(task=task, name=tag)
        Tag.objects.filter(~Q(name__in=tags), task=task).delete()

        task.save()

    @staticmethod
    def copy_task(user, task_id, repeat, index):
        task = Task.objects.get(pk=task_id)
        delta = rel_delta(days=0)
        date = task.end
        i = 0
        while i < index:
            i += 1
            curr_delta = repeat.get_next_repeat(date)
            delta += curr_delta
            date += curr_delta
        trc = TaskRepeatConnection.objects.create(repeat=repeat, index=index)

        trc.save()
        task.id = None
        task.repeat = trc
        task.status = 0
        task.result = Task.INITIAL_RESULT
        try:
            task.start += delta
        except TypeError:
            pass
        task.end += delta
        task.save()
        for notification in task.notification_set.all():
            copied = Notification.objects.create(task=task,
                                                 datetime=notification.datetime+delta,
                                                 target=notification.target,
                                                 time_type=notification.time_type,
                                                 delta=notification.delta,
                                                 text=notification.text,
                                                 copy_of=notification)
            copied.save()
        return task

    @staticmethod
    def get_exclusions_of(user, task_id):
        task = Task.objects.get(pk=task_id)
        try:
            repeat = task.repeat_of
        except ObjectDoesNotExist:
            return {}
        exclusions = {_task.repeat: _task for _task in Task.objects.filter(repeat__repeat=repeat).filter(
                      ~Q(repeat__rep_type=TaskRepeatConnection.NOT_CHANGED_REPEAT_TYPE))}
        exclusions.update({connection: None for connection in
                           TaskRepeatConnection.objects.filter(repeat=repeat,
                                                               rep_type=TaskRepeatConnection.DELETED_REPEAT_TYPE)})
        return exclusions

    @staticmethod
    def get_public_task_connections(current_user, chosen_user, task_id, existing_connection=None):
        if current_user == chosen_user:
            tasks = Task.objects.filter(owner=current_user, repeat=None).all()
        else:
            tasks = Task.objects.filter(owner=chosen_user, repeat=None).filter(
                Q(scope=Task.SCOPE_PUBLIC_SHORT) | Q(assigned=current_user))
        tasks = tasks.filter(~Q(pk=task_id))

        if existing_connection is None:
            tasks = tasks.exclude(sent_connections__receiver=task_id).exclude(received_connections__sender=task_id)
        else:
            tasks = tasks.exclude(~Q(sent_connections=existing_connection), sent_connections__receiver=task_id)\
                .exclude(~Q(received_connections=existing_connection), received_connections__sender=task_id)
        return tasks

    @staticmethod
    def get_subtask_connections(current_user, chosen_user, task_id):
        if current_user == chosen_user:
            tasks = Task.objects.filter(owner=current_user, repeat=None).all()
        else:
            tasks = Task.objects.filter(owner=chosen_user, assigned=current_user, repeat=None)
        tasks = tasks.filter(~Q(pk=task_id))
        root = SubtaskConnection.objects.filter(sender__id=task_id).all()
        print(root)
        if root:
            tasks = tasks.exclude(parent_connection__root=root[0].root)
        tasks = tasks.exclude(parent_connection__root__id=task_id)
        return tasks

    # Notifications

    @staticmethod
    def delete_notification(user, notification_id):
        # TODO check permission
        Notification.objects.filter(pk=notification_id).delete()

    @staticmethod
    def get_notification(user, notification_id):
        # TODO check permission
        return Notification.objects.get(pk=notification_id)

    # Repeats

    @staticmethod
    def add_repeat(user, task_id, rule_type, end=None, relativedelta=None, every_choices=None, i_th_i=None,
                   i_th_week_day=None):
        def copy_task(inner_user, inner_task_id, repeat, inner_delta, index):
            trc = TaskRepeatConnection.objects.create(repeat=repeat, index=index)
            trc.save()
            inner_task = Task.objects.get(pk=inner_task_id)
            inner_task.id = None
            inner_task.repeat = trc
            inner_task.status = 0
            inner_task.result = Task.INITIAL_RESULT
            inner_task.save()
            try:
                try:
                    inner_task.start += delta
                except TypeError:
                    pass
                try:
                    inner_task.end += delta
                except TypeError:
                    pass
                inner_task.save()
            except ObjectDoesNotExist:
                pass
            for notification in Task.objects.get(pk=inner_task_id).notification_set.all():
                copied = Notification.objects.create(task=inner_task,
                                                     datetime=notification.datetime + inner_delta,
                                                     target=notification.target,
                                                     time_type=notification.time_type,
                                                     delta=notification.delta,
                                                     text=notification.text,
                                                     copy_of=notification)
                copied.save()
            return inner_task

        task = Task.objects.get(pk=task_id)
        if rule_type == Repeat.RELATIVE_DELTA_SHORT:
            rule_value = str(relativedelta).split('(')[1][:-1]
            rule_human_readable = 'Repeats with delta ' + \
                                  ', '.join([s.split('=')[1] + ' ' + s.split('=')[0]
                                             for s in
                                             str(relativedelta).split('(')[1][:-1].replace('+', '').split(',')])
        elif rule_type == Repeat.EVERY_SHORT:
            rule_value = every_choices
            rule_human_readable = 'Repeats every ' + Repeat.get_full_rule_type(rule_value)
        elif rule_type == Repeat.LAST_DAY_SHORT:
            rule_value = ''
            rule_human_readable = 'Repeats at last day of every month'
        elif rule_type == Repeat.I_TH_SHORT:
            rule_value = i_th_i + ' ' + i_th_week_day
            rule_human_readable = 'Repeats on the ' + str(i_th_i) + ' ' + i_th_week_day + ' of month'
        else:
            raise ValueError(msg='Unexpected rule_value')
        Repeat.objects.create(template_task=task, end=end, current_index=0, rule_type=rule_type, rule_value=rule_value,
                              rule_human_readable=rule_human_readable)

        # count copies
        i = 0
        if task.notification_set.all().exists():
            first_notif_date = min(task.notification_set.all(), key=lambda notification: notification.datetime).datetime
            while first_notif_date < timezone.now():
                i += 1
                first_notif_date += task.repeat_of.get_next_repeat(task.end)
        # create copies
        delta = rel_delta(days=0)
        copied_task = copy_task(user, task_id, task.repeat_of, delta, 0)
        for j in range(1, i + 1):
            delta += copied_task.repeat.repeat.get_next_repeat(copied_task.end)
            copied_task = copy_task(user, task_id, task.repeat_of, delta, j)

    @staticmethod
    def get_repeat(user, task_id):
        repeat = Repeat.objects.get(template_task__pk=task_id)
        if repeat.rule_type == Repeat.RELATIVE_DELTA_SHORT:
            rd_dict = {s.split('=')[0].strip(): int(s.split('=')[1].strip()) for s in repeat.rule_value.split(',')}
            repeat.rule_value = rel_delta(**rd_dict)
        return repeat

    @staticmethod
    def edit_repeat(user, task_id, rule_type, end=None, relativedelta=None, every_choices=None, i_th_i=None,
                    i_th_week_day=None):
        repeat = Repeat.objects.get(template_task__pk=task_id)
        if rule_type == Repeat.RELATIVE_DELTA_SHORT:
            rule_value = str(relativedelta).split('(')[1][:-1]
            rule_human_readable = 'Repeats with delta ' + \
                                  ', '.join([s.split('=')[1] + ' ' + s.split('=')[0]
                                             for s in
                                             str(relativedelta).split('(')[1][:-1].replace('+', '').split(',')])
        elif rule_type == Repeat.EVERY_SHORT:
            rule_value = every_choices
            rule_human_readable = 'Repeats every ' + Repeat.get_full_rule_type(every_choices)
        elif rule_type == Repeat.LAST_DAY_SHORT:
            rule_value = ''
            rule_human_readable = 'Repeats at last day of every month'
        elif rule_type == Repeat.I_TH_SHORT:
            rule_value = i_th_i + ' ' + i_th_week_day
            rule_human_readable = 'Repeats on the ' + str(i_th_i) + ' ' + i_th_week_day + ' of month'
        else:
            raise ValueError('Unexpected rule_type value')
        repeat.rule_type = rule_type
        repeat.end = end
        repeat.rule_value = rule_value
        repeat.rule_human_readable = rule_human_readable
        repeat.save()
        # check exclusions
        for connection in repeat.taskrepeatconnection_set.all():
            if connection.rep_type != TaskRepeatConnection.NOT_CHANGED_REPEAT_TYPE:
                new_index = repeat.check_date_belongs(repeat.template_task.end, connection.task.end.date())
                if new_index is not None:
                    connection.index = new_index
                    connection.save()
                else:
                    connection.delete()
        # get required copies count
        i = 0
        import pytz
        start = repeat.template_task.end
        while start.replace(tzinfo=pytz.UTC) < datetime.utcnow().replace(tzinfo=pytz.UTC):
            start += repeat.get_next_repeat(start)
            i += 1
        # create copies
        for index in range(i):
            if not TaskRepeatConnection.objects.filter(repeat=repeat, index=index).exists():
                System.copy_task(user, task_id, repeat, index)

    @staticmethod
    def delete_repeat(user, task_id):
        Repeat.objects.get(pk__id=task_id).delete()

    @staticmethod
    def check_belongs(user, task_id, date):
        task = Task.objects.get(pk=task_id)
        try:
            index = task.repeat_of.check_date_belongs(task.end, date)
            return index
        except ObjectDoesNotExist:
            return

    # Exclusions

    @staticmethod
    def add_exclusion(user, task_id, index, name, exclusion_type, description=None, status=None, result=None,
                      priority=None, tags=None, assigned=None, start_message_sent=False):
        if exclusion_type == TaskRepeatConnection.EDITED_REPEAT_TYPE:
            template_task = Task.objects.get(pk=task_id)
            repeat = Repeat.objects.get(template_task=template_task)
            if Task.objects.filter(repeat__repeat=repeat).filter(repeat__index=index).exists():
                System.update_repeat(Task.objects.filter(repeat__repeat=repeat).get(repeat__index=index),
                                     name=name, description=description, status=status, result=result, priority=priority,
                                     tags=tags, assigned=assigned)
            else:
                task = System.copy_task(user, task_id, template_task.repeat_of, index)
                System.update_repeat(task, name=name, description=description, status=status, result=result,
                                     priority=priority, tags=tags, assigned=assigned)
        elif exclusion_type == TaskRepeatConnection.DELETED_REPEAT_TYPE:
            repeat = Repeat.objects.get(template_task__pk=task_id)
            TaskRepeatConnection.objects.filter(repeat=repeat, index=index).delete()
            TaskRepeatConnection.objects.create(repeat=repeat, index=index,
                                                rep_type=TaskRepeatConnection.DELETED_REPEAT_TYPE).save()

    @staticmethod
    def restore_exclusion(user, task_id, index):
        task = Task.objects.get(pk=task_id)
        repeat = task.repeat_of
        TaskRepeatConnection.objects.filter(repeat=repeat, index=index).delete()
        if repeat.current_index >= index:
            System.copy_task(user, task_id, repeat, index)

    # Comments

    @staticmethod
    def add_comment(user, task_id, text):
        task = Task.objects.get(pk=task_id)
        Comment.objects.create(user=user, task=task, text=text, time=datetime.now())

    @staticmethod
    def check_notifications():
        for notification in Notification.objects.filter(is_sent=False, datetime__lt=datetime.now(),
                                                        task__repeat_of=None):
            notification.send()

    @staticmethod
    def check_timelimits():
        for task in Task.objects.filter(status__lt=consts.MAX_STATUS, end__lt=datetime.now(), repeat_of=None):
            task.end_timelimit_hit()
