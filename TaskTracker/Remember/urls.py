from django.conf.urls import include, url

from .views import *
import django.conf.urls

app_name = 'Remember'
urlpatterns = [
    url(r'^hello$', foo, name='hello'), 
    url(r'^$', index, name='index'),
    url(r'^logout$', logout_view, name='logout'),
    url(r'^signup$', SignUpView.as_view(), name='signup'),
    url(r'^login', LoginView.as_view(), name='login'),
    url(r'^profile', ProfileView.as_view(), name='profile'),
    url('^calendar$', CalendarView.as_view(), name='calendar'),
    url('^dashboard$', dashboard, name='dashboard'),
    url('^task/', include([
        url('^new/$', TaskCreateView.as_view(), name='task_new'),
        url('(?P<task_id>\d+)/', include([
            url('^details/$', task_details, name='task_details'),
            url('^edit/', include([
                url('^$', TaskUpdateView.as_view(), name='task_edit'),
                url('^complex$', edit_task_complex, name='task_edit_complex'),
                url('^notification/', include([
                    url('(?P<notification_id>\d+)', NotificationUpdateView.as_view(), name='notification_edit'),
                    url('new$', NotificationCreateView.as_view(), name='notification_new')
                ])),
                url('repeat/', include([
                    url('create$', RepeatCreateView.as_view(), name='repeat_new'),
                    url('edit', RepeatUpdateView.as_view(), name='repeat_edit'),
                    url('add_exclusion', ExclusionCreateView.as_view(), name='exclusion_add')
                ])),
                url('^connection/', include([
                    url('subtask/(?P<user_username>.+)/edit', SubConnectionUpdateView.as_view(), name='subtask_edit'),
                    url('subtask/(?P<user_username>.+)$', SubConnectionCreateView.as_view(), name='subtask'),
                    url('create/(?P<user_username>[a-zA-Z0-9]+)$', ConnectionCreateView.as_view(), name='connection_new'),
                    url('create/(?P<user_username>.+)/(?P<receiver_id>\d+)', ConnectionUpdateView.as_view(),
                        name='connection_edit'),
                ])),
            ])),
        ])),
    ]))
]


django.conf.urls.handler404 = 'Remember.views.handler404'
django.conf.urls.handler500 = 'Remember.views.handler500'
django.conf.urls.handler403 = 'Remember.views.handler403'
django.conf.urls.handler400 = 'Remember.views.handler400'
