from datetime import datetime
from datetime import timedelta

from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import User
from django.test import TestCase

from .models.task import *


class RepeatModelTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='Remember', email='Remember@re.com', password='pswd')
        self.task = Task.objects.create(owner=self.user, name='Task name', end=datetime(2018, 1, 1, 12, 0, 0),
                                        priority=0)
        self.datetime_one = datetime(2018, 1, 1, 12, 0, 0)

    def get_next_and_add(self, repeat, date):
        delta_first = repeat.get_next_repeat(date)
        date += delta_first
        return delta_first, date

    def test_last_day_rule(self):
        date = datetime(2018, 1, 1, 12, 0, 0)
        repeat = Repeat.objects.create(template_task=self.task,
                                       rule_type=Repeat.LAST_DAY_SHORT,
                                       rule_value='')
        deltas = [30, 28, 31]
        for delta_value in deltas:
            delta, date = self.get_next_and_add(repeat, date)
            self.assertEquals(delta, timedelta(days=delta_value))

    def test_i_th_rule(self):
        date = datetime(2018, 1, 1, 12, 0, 0)
        repeat = Repeat.objects.create(template_task=self.task,
                                       rule_type=Repeat.I_TH_SHORT,
                                       rule_value='Mo 3')
        deltas = [14, 35, 28]
        for delta_value in deltas:
            delta, date = self.get_next_and_add(repeat, date)
            self.assertEquals(delta, timedelta(days=delta_value))

    def test_relative_rule_first(self):
        date = datetime(2018, 1, 1, 12, 0, 0)
        repeat = Repeat.objects.create(template_task=self.task,
                                       rule_type=Repeat.RELATIVE_DELTA_SHORT,
                                       rule_value='days=+14')
        delta_value = relativedelta(days=14)
        for i in range(10):
            delta, date = self.get_next_and_add(repeat, date)
            self.assertEquals(delta, delta_value)

    def test_relative_rule_second(self):
        date = datetime(2018, 1, 1, 12, 0, 0)
        repeat = Repeat.objects.create(template_task=self.task,
                                       rule_type=Repeat.RELATIVE_DELTA_SHORT,
                                       rule_value='days=+1, months=+1')
        delta_value = relativedelta(months=+1, days=1)
        for i in range(10):
            delta, date = self.get_next_and_add(repeat, date)
            self.assertEquals(delta, delta_value)

    def test_work_day_rule(self):
        date = datetime(2018, 1, 1, 12, 0, 0)
        repeat = Repeat.objects.create(template_task=self.task,
                                       rule_type=Repeat.EVERY_SHORT,
                                       rule_value=Repeat.WORK_DAYS_CHOICE_SHORT)
        deltas = [1, 1, 1, 1, 3, 1, 1]
        for delta_value in deltas:
            delta, date = self.get_next_and_add(repeat, date)
            self.assertEquals(delta, timedelta(days=delta_value))

    def test_week_end_rule(self):
        date = datetime(2018, 1, 1, 12, 0, 0)
        repeat = Repeat.objects.create(template_task=self.task,
                                       rule_type=Repeat.EVERY_SHORT,
                                       rule_value=Repeat.WEEKENDS_CHOICE_SHORT)
        deltas = [5, 1, 6, 1]
        for delta_value in deltas:
            delta, date = self.get_next_and_add(repeat, date)
            self.assertEquals(delta, timedelta(days=delta_value))

    def test_daily_rule(self):
        date = datetime(2018, 1, 1, 12, 0, 0)
        repeat = Repeat.objects.create(template_task=self.task,
                                       rule_type=Repeat.EVERY_SHORT,
                                       rule_value=Repeat.DAILY_CHOICE_SHORT)
        delta_value = relativedelta(days=1)
        for i in range(10):
            delta, date = self.get_next_and_add(repeat, date)
            self.assertEquals(delta, delta_value)

    def test_weekly_rule(self):
        date = datetime(2018, 1, 1, 12, 0, 0)
        repeat = Repeat.objects.create(template_task=self.task,
                                       rule_type=Repeat.EVERY_SHORT,
                                       rule_value=Repeat.WEEKLY_CHOICE_SHORT)
        delta_value = relativedelta(days=7)
        for i in range(10):
            delta, date = self.get_next_and_add(repeat, date)
            self.assertEquals(delta, delta_value)

    def test_monthly_rule(self):
        date = datetime(2018, 1, 1, 12, 0, 0)
        repeat = Repeat.objects.create(template_task=self.task,
                                       rule_type=Repeat.EVERY_SHORT,
                                       rule_value=Repeat.MONTHLY_CHOICE_SHORT)
        delta_value = relativedelta(months=1)
        for i in range(10):
            delta, date = self.get_next_and_add(repeat, date)
            self.assertEquals(delta, delta_value)