import django.contrib.auth.models as auth_models
from django.db import models

from ..config import CHOICES_FIELD_MAX_LENGTH, MAX_TEXT_FIELD_LENGTH


class Dashboard(models.Model):
    NOTIFICATION_REASON_SHORT = 'N'
    ASSIGN_REASON_SHORT = 'A'
    TIME_LIMIT_REASON_SHORT = 'T'

    REASONS = (
        (NOTIFICATION_REASON_SHORT, 'Notification'),
        (ASSIGN_REASON_SHORT, 'Assign'),
        (TIME_LIMIT_REASON_SHORT, 'Time limit')
    )

    user = models.ForeignKey(auth_models.User, on_delete=models.CASCADE)
    text = models.CharField(max_length=MAX_TEXT_FIELD_LENGTH)
    reason = models.CharField(max_length=CHOICES_FIELD_MAX_LENGTH, choices=REASONS)
    datetime = models.DateTimeField(auto_now_add=True)
