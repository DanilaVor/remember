from dateutil.relativedelta import relativedelta
from django.db import models

from .dashboard import Dashboard
from .task import Task
from .. import consts
from ..config import CHOICES_FIELD_MAX_LENGTH, MAX_TEXT_FIELD_LENGTH


class Notification(models.Model):
    EMAIL_SUBJECT = 'Remember! notification'
    __CHOICES_LENGTH = 1
    DEFAULT_TEXT = 'Task {task_name} hit the notification time.'
    EMAIL_TARGET_SHORT = consts.NOTIFICATION_EMAIL_TARGET[:__CHOICES_LENGTH]
    DASHBOARD_TARGET_SHORT = consts.NOTIFICATION_DASHBOARD_TARGET[:__CHOICES_LENGTH]
    TARGETS = (
        (EMAIL_TARGET_SHORT, consts.NOTIFICATION_EMAIL_TARGET),
        (DASHBOARD_TARGET_SHORT, consts.NOTIFICATION_DASHBOARD_TARGET)
    )
    ABSOLUTE_SHORT = consts.ABSOLUTE_TIME_TYPE[:__CHOICES_LENGTH]
    RELATIVE_SHORT = 'R'

    __TIME_TYPES = (
        (ABSOLUTE_SHORT, consts.ABSOLUTE_TIME_TYPE),
        (RELATIVE_SHORT, consts.RELATIVE_TIME_TYPE)
    )

    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    copy_of = models.ForeignKey('Notification',
                                on_delete=models.CASCADE,
                                related_name='copies',
                                null=True)
    target = models.CharField(max_length=CHOICES_FIELD_MAX_LENGTH, choices=TARGETS)
    time_type = models.CharField(max_length=CHOICES_FIELD_MAX_LENGTH, choices=__TIME_TYPES)
    text = models.CharField(max_length=MAX_TEXT_FIELD_LENGTH, null=True, blank=True)
    datetime = models.DateTimeField()
    delta = models.DurationField(null=True, blank=True)
    is_sent = models.BooleanField(default=False)

    def send(self):
        if self.target == Notification.DASHBOARD_TARGET_SHORT:
            text = self.text
            if not text:
                text = Notification.DEFAULT_TEXT.format(task_name=self.task.name)
            Dashboard.objects.create(user=self.task.owner, text=text, reason=Dashboard.NOTIFICATION_REASON_SHORT)
        elif self.target == Notification.EMAIL_TARGET_SHORT:
            text = self.text
            if not text:
                text = Notification.DEFAULT_TEXT.format(task_name=self.task.name)
            from django.core.mail import send_mail
            from django.conf import settings
            email_from = settings.EMAIL_HOST_USER
            send_mail(Notification.EMAIL_SUBJECT, text, settings.EMAIL_HOST_USER, [self.task.owner.email])
        self.is_sent = True
        self.save()

    def update_copies(self):
        def subtract_dt(dt1, dt2):
            return relativedelta(years=dt1.year - dt2.year, months=dt1.month - dt2.month, days=dt1.day - dt2.day)
        delta = subtract_dt(self.task.end, self.datetime)
        for notification in self.copies.all():
            notification.delta = self.delta
            notification.text = self.text
            notification.time_type = self.time_type
            notification.target = self.target
            notification.datetime = notification.task.end - delta
            notification.save()

    def add_to_repeats(self):
        for task in Task.objects.filter(repeat__repeat__template_task=self.task):
            delta = task.end - self.task.end
            copied = Notification.objects.create(task=task,
                                                 datetime=self.datetime + delta,
                                                 target=self.target,
                                                 time_type=self.time_type,
                                                 delta=self.delta,
                                                 text=self.text,
                                                 copy_of=self)
            copied.save()
