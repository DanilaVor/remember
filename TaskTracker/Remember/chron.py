from django_cron import CronJobBase, Schedule

from .models.task import Task


class MyCronJob(CronJobBase):
    RUN_EVERY_MINS = 1

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'my_app.my_cron_job'    # a unique code

    def do(self):
        with open('llog.txt', 'a') as file:
            s = 'Cron' + str(Task.objects.all().count())
            file.write(s)
        print('IM HERE')