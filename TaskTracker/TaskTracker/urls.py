from django.conf.urls import include, url
from django.contrib import admin
from django.urls import reverse
from django.views.generic import RedirectView

urlpatterns = [
    url(r'remember/', include('Remember.urls'), name='remember'),
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='remember/', permanent=False))
]
